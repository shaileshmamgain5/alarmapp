package in.mamga.quickalarm.activealarms;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.kyleduo.switchbutton.SwitchButton;

import java.util.List;

import in.mamga.quickalarm.alarmhandlers.AlarmController;
import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.databinding.ItemAlarmBinding;

public class AlarmsRecyclerAdapter extends RecyclerView.Adapter<AlarmsRecyclerAdapter.AlarmsRecyclerViewHolder> {

    List<AlarmItem> alarmItems;

    public AlarmsRecyclerAdapter(List<AlarmItem> alarmItems) {
        this.alarmItems = alarmItems;
    }

    @Override
    public AlarmsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(parent.getContext());
        ItemAlarmBinding itemAlarmBinding =
                ItemAlarmBinding.inflate(inflator, parent, false);
        return new AlarmsRecyclerViewHolder(itemAlarmBinding);
    }

    @Override
    public void onBindViewHolder(AlarmsRecyclerViewHolder holder, int position) {
        holder.bind(alarmItems.get(position), position);
        Log.i("Adapter", "Layout inflated == " + position);
    }

    @Override
    public int getItemCount() {
        return alarmItems.size();
    }

    public class AlarmsRecyclerViewHolder extends RecyclerView.ViewHolder {

        private final ItemAlarmBinding itemAlarmBinding;
        SwitchButton sbActive;

        public AlarmsRecyclerViewHolder(ItemAlarmBinding itemAlarmBinding) {
            super(itemAlarmBinding.getRoot());
            this.itemAlarmBinding = itemAlarmBinding;
        }

        public void bind(final AlarmItem alarmItem, final int position) {
            final AlarmItemViewModel viewModel = new AlarmItemViewModel(alarmItem);
            itemAlarmBinding.setAlarmItemViewModel(viewModel);

            itemAlarmBinding.sbActive.setChecked(viewModel.isActive);
            itemAlarmBinding.sbActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    viewModel.setActive(isChecked, buttonView.getContext());
                    viewModel.updateIsRepeat();
                }
            });

            itemAlarmBinding.trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DbUtil.deleteAlarm(alarmItem);
                    alarmItems.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, alarmItems.size());
                }
            });

            itemAlarmBinding.executePendingBindings();
        }
    }
}