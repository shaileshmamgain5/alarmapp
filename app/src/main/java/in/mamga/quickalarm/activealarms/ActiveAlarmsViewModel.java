package in.mamga.quickalarm.activealarms;

import android.content.Context;
import android.databinding.BaseObservable;

import java.util.ArrayList;
import java.util.List;

import in.mamga.quickalarm.dashboard.DashBoardViewModel;
import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DbUtil;


/**
 * Created by Shailesh on 14/03/17.
 */

public class ActiveAlarmsViewModel extends BaseObservable {

    List<AlarmItem> alarms;

    public ActiveAlarmsViewModel() {
        alarms = new ArrayList<AlarmItem>();
        tryCache();
    }

    private void tryCache() {

        List<AlarmItem> alarmItems = DbUtil.getAllAlarms();
        if (alarmItems != null) {
            alarms = alarmItems;
        }
    }

    public List<AlarmItem> getAlarms() {
        return alarms;
    }
}
