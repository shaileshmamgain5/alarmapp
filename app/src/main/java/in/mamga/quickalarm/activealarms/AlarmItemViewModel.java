package in.mamga.quickalarm.activealarms;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.Calendar;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.alarmhandlers.AlarmController;
import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.utils.Util;

/**
 * Created by shailesh on 07/06/17.
 */

public class AlarmItemViewModel extends BaseObservable {

    public static final String TAG = "AlarmItemViewModel";
    @Bindable
    public int hours;
    @Bindable
    public int minutes;
    @Bindable
    public boolean isAm;
    @Bindable
    public boolean isSun;
    @Bindable
    public boolean isMon;
    @Bindable
    public boolean isTue;
    @Bindable
    public boolean isWed;
    @Bindable
    public boolean isThu;
    @Bindable
    public boolean isFri;
    @Bindable
    public boolean isSat;
    @Bindable
    boolean isActive;



    @Bindable
    public String timeInString;

    @Bindable
    public String minString;
    @Bindable
    public String hourString;


    @Bindable
    public boolean isSetAlarmOnTimeRefreshing;

    public AlarmItem alarmItem;


    public AlarmItemViewModel(AlarmItem alarmItem) {
        if (alarmItem ==null)
            this.alarmItem = new AlarmItem();
        else
            this.alarmItem = alarmItem;

        if (alarmItem.getSettingTime() > 0) {
            setUpUI(this.alarmItem);
        } else {
            this.alarmItem.setSettingTime(System.currentTimeMillis());
            initializeLayout();
        }

    }

    private void setUpUI(AlarmItem alarmItem) {
        setHours(alarmItem.getHour());
        setMinutes(alarmItem.getMin());
        setMinutes(alarmItem.getMin());
        setAm(alarmItem.getIsAm());
        setSun(alarmItem.getIsSun());
        setMon(alarmItem.getIsMon());
        setTue(alarmItem.getIsTue());
        setFri(alarmItem.getIsFri());
        setWed(alarmItem.getIsWed());
        setThu(alarmItem.getIsThu());
        setSat(alarmItem.getIsSat());
        isActive = alarmItem.getIsActive();
    }

    private void initializeLayout() {
        Calendar cal = Calendar.getInstance();
        setHours(cal.get(Calendar.HOUR));
        int m = cal.get(Calendar.MINUTE);
        if (QuickAlarmApp.getState().settings().snap()) {
            m = (int) (Math.round(m / 5.0) * 5) % 60;
        }
        setMinutes(m);

        setAm((cal.get(Calendar.HOUR_OF_DAY) >= 12) ? false : true);

    }

    private void updateTimeString() {
        String h = hours < 10 ? "0" + hours : "" + hours;
        String m = minutes < 10 ? "0" + minutes : "" + minutes;
        setMinString(m);
        setHourString(h);

        String time = h + ":" + m + (isAm ? " AM" : " PM");
        setTimeInString(time);

    }


    public void setSetAlarmOnTimeRefreshing(Boolean setAlarmOnTimeRefreshing) {
        isSetAlarmOnTimeRefreshing = setAlarmOnTimeRefreshing;
        notifyPropertyChanged(BR.isSetAlarmOnTimeRefreshing);
    }


    /**
     * Called when view holder fragment or activity is stopped.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onStop(@NonNull String tag) {
    }

    /**
     * Called when view holder fragment or activity is destroyed.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onDestroy(@NonNull String tag) {
        // this.context = null;
    }

    public void setHours(int hours) {
        this.hours = hours;
        alarmItem.setHour(hours);
        updateTimeString();
        notifyPropertyChanged(BR.hours);
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
        alarmItem.setMin(minutes);
        updateTimeString();
        notifyPropertyChanged(BR.minutes);
    }

    public void setAm(boolean am) {
        isAm = am;
        alarmItem.setIsAm(isAm);
        notifyPropertyChanged(BR.isAm);
    }



    public void setSetAlarmOnTimeRefreshing(boolean setAlarmOnTimeRefreshing) {
        isSetAlarmOnTimeRefreshing = setAlarmOnTimeRefreshing;
        notifyPropertyChanged(BR.isSetAlarmOnTimeRefreshing);
    }

    public void setTimeInString(String timeInString) {
        this.timeInString = timeInString;
        notifyPropertyChanged(BR.timeInString);
    }

    public void setMinString(String minString) {
        this.minString = minString;
        notifyPropertyChanged(BR.minString);
    }

    public void setHourString(String hourString) {
        this.hourString = hourString;
        notifyPropertyChanged(BR.hourString);
    }

    public void updateIsRepeat() {

    }
    public void setActive(boolean isActive, Context context){
        alarmItem.setIsActive(isActive);
        alarmItem.setNextAlarm(Util.calculateNextAlarm(alarmItem));
        DbUtil.insertOrUpdateAlarm(alarmItem);
        AlarmController.onAlarmEditedTasks(context);
    }

    public void setSun(boolean sun) {
        isSun = sun;
        alarmItem.setIsSun(isSun);
        updateIsRepeat();
        notifyPropertyChanged(BR.isSun);
    }
    public void setMon(boolean mon) {
        isMon = mon;
        alarmItem.setIsMon(isMon);
        updateIsRepeat();
        notifyPropertyChanged(BR.isMon);
    }

    public void setTue(boolean tue) {
        isTue = tue;
        alarmItem.setIsTue(isTue);
        updateIsRepeat();
        notifyPropertyChanged(BR.isTue);
    }
    public void setWed(boolean wed) {
        isWed = wed;
        alarmItem.setIsWed(isWed);
        updateIsRepeat();
        notifyPropertyChanged(BR.isWed);
    }

    public void setThu(boolean thu) {
        isThu = thu;
        alarmItem.setIsThu(isThu);
        updateIsRepeat();
        notifyPropertyChanged(BR.isThu);
    }

    public void setFri(boolean fri) {
        isFri = fri;
        alarmItem.setIsFri(isFri);
        updateIsRepeat();
        notifyPropertyChanged(BR.isFri);
    }


    public void setSat(boolean sat) {
        isSat = sat;
        alarmItem.setIsSat(isSat);
        updateIsRepeat();
        notifyPropertyChanged(BR.isSat);
    }

}
