package in.mamga.quickalarm.activealarms;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import in.mamga.quickalarm.R;
import in.mamga.quickalarm.databinding.FragmentActiveAlarmsBinding;

/**
 * Created by Shailesh on 15/03/17.
 */

public class ActiveAlarmsFragment extends Fragment {
    public static final String TAG = "ActiveAlarmsFragment";


    ActiveAlarmsViewModel activeAlarmsViewModel;


    RecyclerView rvActiveAlarms;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_alarms, parent, false);

        FragmentActiveAlarmsBinding activeAlarmsBinding = DataBindingUtil.bind(view);

        activeAlarmsViewModel = new ActiveAlarmsViewModel();

        Bundle bundle = getArguments();
        if (bundle != null) {

        }
        activeAlarmsBinding.setActiveAlarmsViewModel(activeAlarmsViewModel);


        rvActiveAlarms = (RecyclerView) view.findViewById(R.id.rvAlarms);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvActiveAlarms.setLayoutManager(layoutManager);
        /*DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(rvActiveAlarms.getContext(),
                layoutManager1.getOrientation());
        rvActiveAlarms.addItemDecoration(dividerItemDecoration1);*/
        AlarmsRecyclerAdapter alarmsRecyclerAdapter = new AlarmsRecyclerAdapter( activeAlarmsViewModel.getAlarms());
        rvActiveAlarms.setAdapter(alarmsRecyclerAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        //activeAlarmsViewModel.onStop(TAG);
    }

}