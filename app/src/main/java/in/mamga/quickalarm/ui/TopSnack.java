package in.mamga.quickalarm.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.androidadvance.topsnackbar.TSnackbar;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import in.mamga.quickalarm.R;

import static android.support.design.widget.SwipeDismissBehavior.SWIPE_DIRECTION_ANY;

/**
 * Created by shailesh on 10/06/17.
 */

public class TopSnack {
    private TSnackbar snackbar;

    private static TopSnack topSnack;
    Context context;
    private static CoordinatorLayout layout;

    private TopSnack() {
    }

    public static TopSnack getInstance(@NonNull CoordinatorLayout layout1) {
        if (topSnack == null) {
            topSnack = new TopSnack();
            layout = layout1;
        }

        return topSnack;
    }

    public void showSnackbar(@NonNull String title, String actionText,@NonNull final OnActionListener onActionListener) {
        dismissSnack();

        snackbar = TSnackbar.make(layout, "", TSnackbar.LENGTH_LONG);
        //final TSnackbar.SnackbarLayout snackbarLayout = (TSnackbar.SnackbarLayout)snackbar.getView();
        snackbar.setText(title);

        if (!TextUtils.isEmpty(actionText)) {
            snackbar.setAction(actionText, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onActionListener.onActionClicked();
                }
            });
        }

        snackbar.show();
    }

    public void dismissSnack() {
        if (snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }



    public interface OnActionListener {
        void onActionClicked();
    }
}
