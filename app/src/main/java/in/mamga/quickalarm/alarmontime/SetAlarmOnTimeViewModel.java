package in.mamga.quickalarm.alarmontime;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.Calendar;
import java.util.List;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.alarmhandlers.AlarmController;
import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.utils.Util;


/**
 * Created by Shailesh on 13/03/17.
 */

public class SetAlarmOnTimeViewModel extends BaseObservable {

    public static final String TAG = "SetAlarmOnTimeViewModel";
    @Bindable
    public int hours;
    @Bindable
    public int minutes;
    @Bindable
    public boolean isAm;
    @Bindable
    public boolean isRepeating;
    @Bindable
    public boolean isSun;
    @Bindable
    public boolean isMon;
    @Bindable
    public boolean isTue;
    @Bindable
    public boolean isWed;
    @Bindable
    public boolean isThu;
    @Bindable
    public boolean isFri;
    @Bindable
    public boolean isSat;


    @Bindable
    public String timeInString;

    @Bindable
    public String minString;
    @Bindable
    public String hourString;


    @Bindable
    public boolean isSetAlarmOnTimeRefreshing;

    public AlarmItem alarmItem;


    public SetAlarmOnTimeViewModel(AlarmItem alarmItem) {
        if (alarmItem == null)
            this.alarmItem = new AlarmItem();
        else
            this.alarmItem = alarmItem;

        if (alarmItem.getSettingTime() > 0) {
            setUpUI(this.alarmItem);
        } else {
            initializeAlarmItem();
        }

    }

    private void setUpUI(AlarmItem alarmItem) {
        setHours(alarmItem.getHour());
        setMinutes(alarmItem.getMin());
        setMinutes(alarmItem.getMin());
        setAm(alarmItem.getIsAm());
        setSun(alarmItem.getIsSun());
        setMon(alarmItem.getIsMon());
        setTue(alarmItem.getIsTue());
        setFri(alarmItem.getIsFri());
        setWed(alarmItem.getIsWed());
        setThu(alarmItem.getIsThu());
        setSat(alarmItem.getIsSat());
    }

    private void initializeAlarmItem() {
        Calendar cal = Calendar.getInstance();
        int m = cal.get(Calendar.MINUTE);
        if (QuickAlarmApp.getState().settings().snap()) {
            m = (int) (Math.round(m / 5.0) * 5) % 60;
        }
        alarmItem.setHour(cal.get(Calendar.HOUR));
        alarmItem.setMin(m);
        alarmItem.setIsAm((cal.get(Calendar.HOUR_OF_DAY) >= 12) ? false : true);
        this.alarmItem.setSettingTime(System.currentTimeMillis());
        setUpUI(alarmItem);
    }

    public void updateTimeString() {
        String h = hours < 10 ? "0" + hours : "" + hours;
        if (hours == 0 && !isAm) {
            h = "12";
        }
        String m = minutes < 10 ? "0" + minutes : "" + minutes;
        setMinString(m);
        setHourString(h);

        String time = h + ":" + m;
        setTimeInString(time);

    }


    public void setSetAlarmOnTimeRefreshing(Boolean setAlarmOnTimeRefreshing) {
        isSetAlarmOnTimeRefreshing = setAlarmOnTimeRefreshing;
        notifyPropertyChanged(BR.isSetAlarmOnTimeRefreshing);
    }


    /**
     * Called when view holder fragment or activity is stopped.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onStop(@NonNull String tag) {
    }

    /**
     * Called when view holder fragment or activity is destroyed.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onDestroy(@NonNull String tag) {
        // this.context = null;
    }

    public void setHours(int hours) {
        this.hours = hours;
        alarmItem.setHour(hours);
        updateTimeString();
        notifyPropertyChanged(BR.hours);
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
        alarmItem.setMin(minutes);
        updateTimeString();
        notifyPropertyChanged(BR.minutes);
    }

    public void setAm(boolean am) {
        isAm = am;
        alarmItem.setIsAm(isAm);
        notifyPropertyChanged(BR.isAm);
    }

    public void setRepeating(boolean repeating) {
        isRepeating = repeating;
        alarmItem.setIsRepeat(isRepeating);
        notifyPropertyChanged(BR.isRepeating);
    }

    public void setSetAlarmOnTimeRefreshing(boolean setAlarmOnTimeRefreshing) {
        isSetAlarmOnTimeRefreshing = setAlarmOnTimeRefreshing;
        notifyPropertyChanged(BR.isSetAlarmOnTimeRefreshing);
    }

    public void setTimeInString(String timeInString) {
        this.timeInString = timeInString;
        notifyPropertyChanged(BR.timeInString);
    }

    public void setMinString(String minString) {
        this.minString = minString;
        notifyPropertyChanged(BR.minString);
    }

    public void setHourString(String hourString) {
            this.hourString = hourString;
        notifyPropertyChanged(BR.hourString);
    }

    public void updateIsRepeat() {
        if (isSun || isMon || isTue || isWed || isThu || isFri || isSat) {
            setRepeating(true);
        } else {
            setRepeating(false);
        }
    }

    public void setSun(boolean sun) {
        isSun = sun;
        alarmItem.setIsSun(isSun);
        updateIsRepeat();
        notifyPropertyChanged(BR.isSun);
    }

    public void toggleSun(View view) {
        setSun(!isSun);
    }

    public void setMon(boolean mon) {
        isMon = mon;
        alarmItem.setIsMon(isMon);
        updateIsRepeat();
        notifyPropertyChanged(BR.isMon);
    }

    public void toggleMon(View view) {
        setMon(!isMon);
    }

    public void setTue(boolean tue) {
        isTue = tue;
        alarmItem.setIsTue(isTue);
        updateIsRepeat();
        notifyPropertyChanged(BR.isTue);
    }

    public void toggleTue(View view) {
        setTue(!isTue);
    }

    public void setWed(boolean wed) {
        isWed = wed;
        alarmItem.setIsWed(isWed);
        updateIsRepeat();
        notifyPropertyChanged(BR.isWed);
    }

    public void toggleWed(View view) {
        setWed(!isWed);
    }

    public void setThu(boolean thu) {
        isThu = thu;
        alarmItem.setIsThu(isThu);
        updateIsRepeat();
        notifyPropertyChanged(BR.isThu);
    }

    public void toggleThu(View view) {
        setThu(!isThu);
    }

    public void setFri(boolean fri) {
        isFri = fri;
        alarmItem.setIsFri(isFri);
        updateIsRepeat();
        notifyPropertyChanged(BR.isFri);
    }

    public void toggleFri(View view) {
        setFri(!isFri);
    }

    public void setSat(boolean sat) {
        isSat = sat;
        alarmItem.setIsSat(isSat);
        updateIsRepeat();
        notifyPropertyChanged(BR.isSat);
    }

    public void toggleSat(View view) {
        setSat(!isSat);
    }

    public void onAlarmSetClicked(View view) {
        alarmItem.setId(System.currentTimeMillis());
        //First checking for duplicate
        List<AlarmItem> alarmItems = DbUtil.getAllAlarms();
        if (alarmItems != null && alarmItems.size() > 0) {
            for (AlarmItem item : alarmItems) {
                if (item.isEqual(alarmItem)) {
                    alarmItem.setId(item.getId());
                    break;
                }
            }
        }

        alarmItem.setSettingTime(System.currentTimeMillis());
        alarmItem.setIsActive(true);
        alarmItem.setNextAlarm(Util.calculateNextAlarm(alarmItem));
        DbUtil.insertOrUpdateAlarm(alarmItem);

        if (onTimeAlarmSetListener != null) {
            onTimeAlarmSetListener.onSet(alarmItem);
        }

        alarmItem = new AlarmItem();
        QuickAlarmApp.setCurrentAlarmItem(alarmItem);
        initializeAlarmItem();

        AlarmController.onAlarmEditedTasks(view.getContext());
        //Todo : remember to set nextAlarm before setting alarm and saving
        //then check for latest next alarm and set alarm manager for the same.
    }

    OnTimeAlarmSetListener onTimeAlarmSetListener;

    public void setOnTimeAlarmSetListener(OnTimeAlarmSetListener onTimeAlarmSetListener) {
        this.onTimeAlarmSetListener = onTimeAlarmSetListener;
    }

    public interface OnTimeAlarmSetListener {
        void onSet(AlarmItem item);
    }

    public void onRepeatClick(View view) {
        if (isRepeating) {
            setSun(false);
            setMon(false);
            setTue(false);
            setWed(false);
            setThu(false);
            setFri(false);
            setSat(false);
        } else {
            setSun(true);
            setMon(true);
            setTue(true);
            setWed(true);
            setThu(true);
            setFri(true);
            setSat(true);
        }
    }


}
