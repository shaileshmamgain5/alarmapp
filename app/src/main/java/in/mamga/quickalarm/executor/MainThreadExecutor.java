package in.mamga.quickalarm.executor;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executor;

/**
 * Created by shailesh on 21/04/17.
 */

public class MainThreadExecutor implements Executor {

    private final Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public void execute(Runnable runnable) {
        handler.post(runnable);
    }

    public void execute(Runnable runnable, long delayMillis) {
        handler.postDelayed(runnable, delayMillis);
    }

    public void removeCallBack(Runnable runnable) {
        handler.removeCallbacks(runnable);
    }
}