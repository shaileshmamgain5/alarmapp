package in.mamga.quickalarm.executor;


import in.mamga.quickalarm.executor.enums.Priority;

/**
 * Created by Shailesh on 21/04/17.
 */

public abstract class PriorityRunnable implements Runnable {

    private final Priority priority;

    public PriorityRunnable(Priority priority) {
        this.priority = priority;
    }

    public Priority getPriority() {
        return priority;
    }

}