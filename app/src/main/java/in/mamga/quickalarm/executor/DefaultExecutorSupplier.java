package in.mamga.quickalarm.executor;

import android.os.Process;

import com.facebook.imagepipeline.core.PriorityThreadFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by shailesh on 21/04/17.
 */

/*
* Singleton class for default executor supplier
*/
public class DefaultExecutorSupplier{
    /*
    * Number of cores to decide the number of threads
    */
    public static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();

    /*
    * thread pool executor for background tasks
    */
    private final PriorityThreadPoolExecutor mForBackgroundTasks;
    /*
    * thread pool executor for light weight background tasks
    */
    private final ThreadPoolExecutor mForLightWeightBackgroundTasks;

    private final ScheduledExecutorService mScheduledSerialExecutorService;
    /*
    * thread pool executor for main thread tasks
    */
    private final MainThreadExecutor mMainThreadExecutor;
    /*
    * an instance of DefaultExecutorSupplier
    */
    private static DefaultExecutorSupplier sInstance;

    /*
   * thread pool executor for foreground tasks
   */
    private final PriorityThreadPoolExecutor mForForeGroundTasks;

    /*
    * returns the instance of DefaultExecutorSupplier
    */
    public static DefaultExecutorSupplier getInstance() {
        if (sInstance == null) {
            synchronized (DefaultExecutorSupplier.class) {
                if (sInstance == null) {
                    sInstance = new DefaultExecutorSupplier();
                }
            }
        }
        return sInstance;
    }

    /*
    * constructor for  DefaultExecutorSupplier
    */
    private DefaultExecutorSupplier() {

        // setting the thread factory
        ThreadFactory backgroundPriorityThreadFactory = new
                PriorityThreadFactory(Process.THREAD_PRIORITY_BACKGROUND);

        // setting the thread factory
        ThreadFactory foregroundPriorityThreadFactory = new
                PriorityThreadFactory(Process.THREAD_PRIORITY_FOREGROUND);

        // setting the thread pool executor for mForBackgroundTasks;
        mForBackgroundTasks = new PriorityThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                backgroundPriorityThreadFactory
        );


        // setting the thread pool executor for mForLightWeightBackgroundTasks;
        mForLightWeightBackgroundTasks = new ThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(),
                backgroundPriorityThreadFactory
        );

        // setting the thread pool executor for mMainThreadExecutor;
        mMainThreadExecutor = new MainThreadExecutor();

        mScheduledSerialExecutorService = Executors.newSingleThreadScheduledExecutor();

        mForForeGroundTasks =  new PriorityThreadPoolExecutor(
                NUMBER_OF_CORES * 2,
                NUMBER_OF_CORES * 2,
                60L,
                TimeUnit.SECONDS,
                foregroundPriorityThreadFactory
        );
    }

    /*
    * returns the thread pool executor for background task
    */
    public ThreadPoolExecutor forBackgroundTasks() {
        return mForBackgroundTasks;
    }

    /*
    * returns the thread pool executor for light weight background task
    */
    public ThreadPoolExecutor forLightWeightBackgroundTasks() {
        return mForLightWeightBackgroundTasks;
    }

    /*
    * returns the thread pool executor for main thread task
    */
    public MainThreadExecutor forMainThreadTasks() {
        return mMainThreadExecutor;
    }

    public ScheduledExecutorService forScheduledSerialExecutorService() {
        return mScheduledSerialExecutorService;
    }

    /*
    * returns the thread pool executor for background task
    */
    public ThreadPoolExecutor forForegroundTasks() {
        return mForForeGroundTasks;
    }

}
