package in.mamga.quickalarm.database;

import com.google.gson.Gson;

import org.greenrobot.greendao.annotation.Convert;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.converter.PropertyConverter;

/**
 * Created by Shailesh on 14/03/17.
 */
@Entity
public class AlarmItem {
    @Id
    public Long id;
    boolean isActive;
    long settingTime;  //time when the alarm was set
    int hour;
    int min;
    boolean isAm;
    boolean isRepeat;
    boolean isSun;
    boolean isMon;
    boolean isTue;
    boolean isWed;
    boolean isThu;
    boolean isFri;
    boolean isSat;
    @Index //Todo : set not null
    long nextAlarm;

    public boolean isEqual(AlarmItem item) {
        if (item.getHour() == hour && item.getMin() == min && item.getIsAm() == isAm
                && item.getIsSun() == isSun && item.getIsMon()== isMon && item.getIsTue() == isTue
                && item.getIsWed() == isWed && item.getIsThu() == isThu && item.getIsFri() == isFri
                && item.getIsSat() == isSat) {
            return true;
        } else {
            return false;
        }
    }

    @Generated(hash = 168314226)
    public AlarmItem(Long id, boolean isActive, long settingTime, int hour, int min,
            boolean isAm, boolean isRepeat, boolean isSun, boolean isMon,
            boolean isTue, boolean isWed, boolean isThu, boolean isFri,
            boolean isSat, long nextAlarm) {
        this.id = id;
        this.isActive = isActive;
        this.settingTime = settingTime;
        this.hour = hour;
        this.min = min;
        this.isAm = isAm;
        this.isRepeat = isRepeat;
        this.isSun = isSun;
        this.isMon = isMon;
        this.isTue = isTue;
        this.isWed = isWed;
        this.isThu = isThu;
        this.isFri = isFri;
        this.isSat = isSat;
        this.nextAlarm = nextAlarm;
    }
    @Generated(hash = 92709898)
    public AlarmItem() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public boolean getIsActive() {
        return this.isActive;
    }
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    public long getSettingTime() {
        return this.settingTime;
    }
    public void setSettingTime(long settingTime) {
        this.settingTime = settingTime;
    }
    public int getHour() {
        return this.hour;
    }
    public void setHour(int hour) {
        this.hour = hour;
    }
    public int getMin() {
        return this.min;
    }
    public void setMin(int min) {
        this.min = min;
    }
    public boolean getIsAm() {
        return this.isAm;
    }
    public void setIsAm(boolean isAm) {
        this.isAm = isAm;
    }
    public boolean getIsRepeat() {
        return this.isRepeat;
    }
    public void setIsRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }
    public boolean getIsSun() {
        return this.isSun;
    }
    public void setIsSun(boolean isSun) {
        this.isSun = isSun;
    }
    public boolean getIsMon() {
        return this.isMon;
    }
    public void setIsMon(boolean isMon) {
        this.isMon = isMon;
    }
    public boolean getIsTue() {
        return this.isTue;
    }
    public void setIsTue(boolean isTue) {
        this.isTue = isTue;
    }
    public boolean getIsWed() {
        return this.isWed;
    }
    public void setIsWed(boolean isWed) {
        this.isWed = isWed;
    }
    public boolean getIsThu() {
        return this.isThu;
    }
    public void setIsThu(boolean isThu) {
        this.isThu = isThu;
    }
    public boolean getIsFri() {
        return this.isFri;
    }
    public void setIsFri(boolean isFri) {
        this.isFri = isFri;
    }
    public boolean getIsSat() {
        return this.isSat;
    }
    public void setIsSat(boolean isSat) {
        this.isSat = isSat;
    }
    public long getNextAlarm() {
        return this.nextAlarm;
    }
    public void setNextAlarm(long nextAlarm) {
        this.nextAlarm = nextAlarm;
    }

}
