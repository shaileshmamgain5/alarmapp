package in.mamga.quickalarm.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by shailesh on 06/08/17.
 */

@Entity
public class NapAlarmItem {
    @Id
    public Long id;
    int duration; //in minutes
    boolean isActive;
    boolean isHeadphoneOnly;
    boolean inNotification;
    Long startTime;
    int usedCounts;
    @Generated(hash = 659500343)
    public NapAlarmItem(Long id, int duration, boolean isActive,
            boolean isHeadphoneOnly, boolean inNotification, Long startTime,
            int usedCounts) {
        this.id = id;
        this.duration = duration;
        this.isActive = isActive;
        this.isHeadphoneOnly = isHeadphoneOnly;
        this.inNotification = inNotification;
        this.startTime = startTime;
        this.usedCounts = usedCounts;
    }
    @Generated(hash = 1934207749)
    public NapAlarmItem() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getDuration() {
        return this.duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public boolean getIsActive() {
        return this.isActive;
    }
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
    public boolean getIsHeadphoneOnly() {
        return this.isHeadphoneOnly;
    }
    public void setIsHeadphoneOnly(boolean isHeadphoneOnly) {
        this.isHeadphoneOnly = isHeadphoneOnly;
    }
    public boolean getInNotification() {
        return this.inNotification;
    }
    public void setInNotification(boolean inNotification) {
        this.inNotification = inNotification;
    }
    public Long getStartTime() {
        return this.startTime;
    }
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    public int getUsedCounts() {
        return this.usedCounts;
    }
    public void setUsedCounts(int usedCounts) {
        this.usedCounts = usedCounts;
    }
}
