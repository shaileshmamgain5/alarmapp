package in.mamga.quickalarm.database;

import android.content.Context;
import android.util.Log;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.util.List;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.utils.StringUtil;
import in.mamga.quickalarm.utils.Util;

/**
 * Created by photography on 23/03/17.
 */

public class DbUtil {/*
    public static boolean updateCurrentWeatherData (String cityId, CurrentWeatherData weatherData) {
        if (weatherData != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            CityWeatherDao cityWeatherDao = daoSession.getCityWeatherDao();
            Query<AlarmItem> cityWeatherQuery;
            List<AlarmItem> list = null;
            if (!TextUtils.isEmpty(cityId))
            {
                QueryBuilder<AlarmItem> queryBuilder = cityWeatherDao.queryBuilder().where(CityWeatherDao.Properties.Id.eq(cityId));
                cityWeatherQuery =  queryBuilder.build();
                list = cityWeatherQuery.list();
            }

            if (list != null && list.size() > 0) {
                AlarmItem alarmItem = list.get(0);
                alarmItem.setCurrentWeatherData(weatherData);
                cityWeatherDao.insertOrReplace(alarmItem);
                return true;
            } else {
                AlarmItem alarmItem = new AlarmItem(weatherData.getId()+"", weatherData.getCoord().getLat(),
                        weatherData.getCoord().getLon(), weatherData, null, (weatherData.getDt() * 1000L), 0L);
                cityWeatherDao.insertOrReplace(alarmItem);
            }

        }
        return false;
    }

    public static CurrentWeatherData getCurrentWeatherCache(Context context) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        CityWeatherDao cityWeatherDao = daoSession.getCityWeatherDao();
        String cityId = SharePreference.getInstance(context).getString(StringUtil.CURRENT_CITY_ID_KEY);
        Query<AlarmItem> cityWeatherQuery;
        List<AlarmItem> list = null;
        if (!TextUtils.isEmpty(cityId))
        {
            QueryBuilder<AlarmItem> queryBuilder = cityWeatherDao.queryBuilder().where(CityWeatherDao.Properties.Id.eq(cityId));
            cityWeatherQuery =  queryBuilder.build();
            list = cityWeatherQuery.list();
        }

        if (list != null && list.size() > 0) {
            return list.get(0).getCurrentWeatherData();
        }
        return null;
    }

    public static WeatherForecastData getCurrentWeatherForecastCache(Context context) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        CityWeatherDao cityWeatherDao = daoSession.getCityWeatherDao();
        String cityId = SharePreference.getInstance(context).getString(StringUtil.CURRENT_CITY_ID_KEY);
        Query<AlarmItem> cityWeatherQuery;
        List<AlarmItem> list = null;
        if (!TextUtils.isEmpty(cityId))
        {
            QueryBuilder<AlarmItem> queryBuilder = cityWeatherDao.queryBuilder().where(CityWeatherDao.Properties.Id.eq(cityId));
            cityWeatherQuery =  queryBuilder.build();
            list = cityWeatherQuery.list();
        }

        if (list != null && list.size() > 0) {
            return list.get(0).getWeatherForecastData();
        }
        return null;
    }

    public static boolean updateWeatherForecastData (String cityId, WeatherForecastData forecastData) {
        if (forecastData != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            CityWeatherDao cityWeatherDao = daoSession.getCityWeatherDao();
            Query<AlarmItem> cityWeatherQuery;
            List<AlarmItem> list = null;
            if (!TextUtils.isEmpty(cityId))
            {
                QueryBuilder<AlarmItem> queryBuilder = cityWeatherDao.queryBuilder().where(CityWeatherDao.Properties.Id.eq(cityId));
                cityWeatherQuery =  queryBuilder.build();
                list = cityWeatherQuery.list();
            }

            if (list != null && list.size() > 0) {
                AlarmItem alarmItem = list.get(0);
                alarmItem.setWeatherForecastData(forecastData);
                cityWeatherDao.insertOrReplace(alarmItem);
                return true;
            } else {
                AlarmItem alarmItem = new AlarmItem(cityId , forecastData.getCity().getCoord().getLat(),
                        forecastData.getCity().getCoord().getLon(), null, forecastData, (forecastData.getList().get(0).getDt() * 1000L), 0L);
                cityWeatherDao.insertOrReplace(alarmItem);
            }

        }
        return false;
    }*/

    public static boolean insertOrUpdateAlarm(AlarmItem alarmItem) {
        if (alarmItem != null && alarmItem.getId() > 0) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();
            alarmItemDao.insertOrReplace(alarmItem);
            return true;
        }
        return false;
    }

    public static boolean deleteAlarm(AlarmItem alarmItem) {
        if (alarmItem != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();

            try {
                alarmItemDao.delete(alarmItem);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }

    public static long getNextAlarm() {
        long nextAlarm = 0;
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();
        Query<AlarmItem> alarmItemQuery;
        List<AlarmItem> list = null;
        QueryBuilder<AlarmItem> queryBuilder = alarmItemDao.queryBuilder().where(AlarmItemDao.Properties.IsActive.eq(1)).
                orderAsc(AlarmItemDao.Properties.NextAlarm);
        alarmItemQuery = queryBuilder.build();
        list = alarmItemQuery.list();

        if (list != null && list.size() > 0) {
            for (AlarmItem item : list) {
                if (item.getNextAlarm() > 0 && item.nextAlarm > System.currentTimeMillis()) {
                    nextAlarm = list.get(0).getNextAlarm();
                    break;
                }
            }
        }

        return nextAlarm;
    }

    public static List<AlarmItem> getAlarmItemsByNextAlarmTime(long nextAlarm) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();
        Query<AlarmItem> alarmItemQuery;
        List<AlarmItem> list = null;
        QueryBuilder<AlarmItem> queryBuilder = alarmItemDao.queryBuilder().where(AlarmItemDao.Properties.NextAlarm.eq(nextAlarm));
        alarmItemQuery = queryBuilder.build();
        list = alarmItemQuery.list();

        return list;
    }

    public static void resetNextAlarms(long nextAlarm) {
        List<AlarmItem> list = getAlarmItemsByNextAlarmTime(nextAlarm);

        if (list != null && list.size() > 0) {
            for (AlarmItem item : list) {
                item.setNextAlarm(Util.calculateNextAlarm(item));
                insertOrUpdateAlarm(item);
            }
        }
    }

    public static List<AlarmItem> getAllAlarms() {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();
        Query<AlarmItem> alarmItemQuery;
        List<AlarmItem> list = null;
        QueryBuilder<AlarmItem> queryBuilder = alarmItemDao.queryBuilder().orderDesc(AlarmItemDao.Properties.SettingTime);
        alarmItemQuery = queryBuilder.build();
        list = alarmItemQuery.list();

        if (list != null && list.size() > 0) {
            return list;
        }
        return null;
    }

    public static void removeLastSetAlarm() {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        AlarmItemDao alarmItemDao = daoSession.getAlarmItemDao();
        Query<AlarmItem> alarmItemQuery;
        List<AlarmItem> list = null;
        QueryBuilder<AlarmItem> queryBuilder = alarmItemDao.queryBuilder().orderDesc(AlarmItemDao.Properties.SettingTime).limit(1);
        alarmItemQuery = queryBuilder.build();
        list = alarmItemQuery.list();

        if (list != null && list.size() > 0) {
            deleteAlarm(list.get(0));
        }

    }


    //################################ NAP ALARM ITEMS ##########################
    public static List<NapAlarmItem> getAllNapAlarmItems() {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        NapAlarmItemDao napAlarmItemDao = daoSession.getNapAlarmItemDao();
        Query<NapAlarmItem> alarmItemQuery;
        List<NapAlarmItem> napAlarmItems = null;
        QueryBuilder<NapAlarmItem> queryBuilder = napAlarmItemDao.queryBuilder().orderAsc(NapAlarmItemDao.Properties.Duration);
        alarmItemQuery = queryBuilder.build();
        napAlarmItems = alarmItemQuery.list();
        return napAlarmItems;
    }

    public static List<NapAlarmItem> getAllActiveNapAlarmItems() {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        NapAlarmItemDao napAlarmItemDao = daoSession.getNapAlarmItemDao();
        Query<NapAlarmItem> alarmItemQuery;
        List<NapAlarmItem> napAlarmItems = null;
        QueryBuilder<NapAlarmItem> queryBuilder = napAlarmItemDao.queryBuilder()
                .where((NapAlarmItemDao.Properties.IsActive).eq(1)).orderAsc(NapAlarmItemDao.Properties.Duration);
        alarmItemQuery = queryBuilder.build();
        napAlarmItems = alarmItemQuery.list();
        return napAlarmItems;
    }

    public static boolean insertOrUpdateNapAlarm(NapAlarmItem alarmItem) {
        if (alarmItem != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            NapAlarmItemDao alarmItemDao = daoSession.getNapAlarmItemDao();
            List<NapAlarmItem> napAlarmItems = getNapAlarmByTime(alarmItem.getDuration());
            if (napAlarmItems != null) {
                Log.i("DbUtil" , "Size : " + napAlarmItems.size());
                deleteNapAlarmList(napAlarmItems);
            }
            alarmItemDao.insertOrReplace(alarmItem);
            return true;
        }
        return false;
    }

    public static boolean deleteNapAlarm(NapAlarmItem alarmItem) {
        if (alarmItem != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            NapAlarmItemDao alarmItemDao = daoSession.getNapAlarmItemDao();
            try {
                alarmItemDao.delete(alarmItem);
                daoSession.clear();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean deleteNapAlarmList(List<NapAlarmItem> alarmItems) {
        if (alarmItems != null) {
            DaoSession daoSession = QuickAlarmApp.getDaoSession();
            NapAlarmItemDao alarmItemDao = daoSession.getNapAlarmItemDao();
            try {
                alarmItemDao.deleteInTx(alarmItems);
                daoSession.clear();
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isNapAlarmExists(NapAlarmItem alarmItem) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        NapAlarmItemDao napAlarmItemDao = daoSession.getNapAlarmItemDao();
        Query<NapAlarmItem> alarmItemQuery;
        List<NapAlarmItem> napAlarmItems = null;
        QueryBuilder<NapAlarmItem> queryBuilder = napAlarmItemDao.queryBuilder()
                .where((NapAlarmItemDao.Properties.IsActive).eq(1)).orderAsc(NapAlarmItemDao.Properties.Duration);
        alarmItemQuery = queryBuilder.build();
        napAlarmItems = alarmItemQuery.list();
        if (napAlarmItems != null && napAlarmItems.size() > 0) {
            for (NapAlarmItem item : napAlarmItems) {
                if (item.getDuration() == alarmItem.getDuration()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static NapAlarmItem getNapAlarmById(Long alarmId) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        NapAlarmItemDao napAlarmItemDao = daoSession.getNapAlarmItemDao();
        Query<NapAlarmItem> alarmItemQuery;
        List<NapAlarmItem> napAlarmItems = null;
        QueryBuilder<NapAlarmItem> queryBuilder = napAlarmItemDao.queryBuilder()
                .where((NapAlarmItemDao.Properties.Id).eq(alarmId)).orderAsc(NapAlarmItemDao.Properties.Duration);
        alarmItemQuery = queryBuilder.build();
        napAlarmItems = alarmItemQuery.list();
        if (napAlarmItems != null && napAlarmItems.size() > 0) {
            return napAlarmItems.get(0);
        }
        return null;
    }

    public static List<NapAlarmItem> getNapAlarmByTime(int duration) {
        DaoSession daoSession = QuickAlarmApp.getDaoSession();
        NapAlarmItemDao napAlarmItemDao = daoSession.getNapAlarmItemDao();
        Query<NapAlarmItem> alarmItemQuery;
        List<NapAlarmItem> napAlarmItems = null;
        QueryBuilder<NapAlarmItem> queryBuilder = napAlarmItemDao.queryBuilder()
                .where((NapAlarmItemDao.Properties.Duration).eq(duration));
        alarmItemQuery = queryBuilder.build();
        napAlarmItems = alarmItemQuery.list();
        if (napAlarmItems != null && napAlarmItems.size() > 0) {
            return napAlarmItems;
        }
        return null;
    }


    public static void initializeNapAlarmsIfFirstTime(Context context) {
        if (!SharePreference.getInstance(context).getBoolean(StringUtil.NAP_ALARMS_INITIALIZED_KEY)) {
            int[] durations = {15, 30, 45};
            for (int i = 0; i < durations.length; i++) {
                NapAlarmItem item = new NapAlarmItem();
                item.setDuration(durations[i]);
                item.setIsActive(false);
                item.setStartTime((long) -1);
                DbUtil.insertOrUpdateNapAlarm(item);
            }
            SharePreference.getInstance(context).putBoolean(StringUtil.NAP_ALARMS_INITIALIZED_KEY, true);
        }

    }
}
