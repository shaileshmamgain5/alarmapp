package in.mamga.quickalarm.settings;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.R;
import in.mamga.quickalarm.dashboard.UpdateClockEvent;
import in.mamga.quickalarm.databinding.FragmentSettingBinding;

import static in.mamga.quickalarm.MainActivity.TONE_PICKER;

/**
 * Created by shailesh on 07/06/17.
 */

public class SettingsFragment extends Fragment {
    public static final String TAG = "SettingsFragment";

    SettingsViewModel settingsViewModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, parent, false);

        FragmentSettingBinding fragmentSettingBinding = DataBindingUtil.bind(view);

        settingsViewModel = new SettingsViewModel(getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {

        }
        fragmentSettingBinding.setSettingsViewModel(settingsViewModel);

        fragmentSettingBinding.cbFade.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (settingsViewModel.isFadeIn != isChecked) {
                    settingsViewModel.setFadeIn(isChecked);
                    settingsViewModel.updateState();
                }
            }
        });

        fragmentSettingBinding.cbVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (settingsViewModel.isVibrate != isChecked) {
                    settingsViewModel.setVibrate(isChecked);
                    settingsViewModel.updateState();
                }
            }
        });

        fragmentSettingBinding.cbSnap.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (settingsViewModel.isSnap != isChecked) {
                    settingsViewModel.setSnap(isChecked);
                    settingsViewModel.updateState();
                }
            }
        });

        LinearLayout llRingTone = (LinearLayout) view.findViewById(R.id.llRingTone);
        llRingTone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Uri currentTone = RingtoneManager.getActualDefaultRingtoneUri(getActivity(), RingtoneManager.TYPE_ALARM);
                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, currentTone);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
                getActivity().startActivityForResult(intent, TONE_PICKER);
            }
        });

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (settingsViewModel != null) {
            settingsViewModel.onStop(TAG);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRingToneChanged(ToneChangedEvent event) {
        if (settingsViewModel != null) {
            settingsViewModel.updateRingToneName();
        }
    }

}