package in.mamga.quickalarm.settings;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.QuickAlarmApp;

/**
 * Created by shailesh on 07/06/17.
 */

public class SettingsViewModel extends BaseObservable {

    public static final String TAG = "SettingsViewModel";
    Context context;


    @Bindable
    public boolean isSettingsRefreshing;
    @Bindable
    public boolean isVibrate;
    @Bindable
    public boolean isSnap;
    @Bindable
    public boolean isFadeIn;
    @Bindable
    public int theme;
    @Bindable
    public String ringtoneName;


    public SettingsViewModel(Context context) {
        this.context = context;

        setUpViewModel();
    }

    private void setUpViewModel() {
        setFadeIn(QuickAlarmApp.getState().settings().ramping());
        setSnap(QuickAlarmApp.getState().settings().snap());
        setVibrate(QuickAlarmApp.getState().settings().vibrate());
        setTheme(QuickAlarmApp.getState().settings().theme());
        updateRingToneName();
    }

    public void updateRingToneName() {
        try {
            Ringtone r = RingtoneManager.getRingtone(context, Uri.parse(QuickAlarmApp.getState().settings().ringtone()));
            String ringToneName = r.getTitle(context);
            setRingtoneName(ringToneName);
        } catch (Exception e) {
            setRingtoneName("Standard");
        }
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public void onStop(@NonNull String TAG) {
    }

    public void onDestroy(@NonNull String tag) {

        context = null;
    }

    public void setSettingsRefreshing(Boolean settingsRefreshing) {
        this.isSettingsRefreshing = settingsRefreshing;
        notifyPropertyChanged(BR.isDashboardRefreshing);
    }

    public void setSettingsRefreshing(boolean settingsRefreshing) {
        this.isSettingsRefreshing = settingsRefreshing;
        notifyPropertyChanged(BR.isSettingsRefreshing);
    }

    public void setVibrate(boolean vibrate) {
        this.isVibrate = vibrate;
        notifyPropertyChanged(BR.isVibrate);
    }

    public void setSnap(boolean snap) {
        this.isSnap = snap;
        notifyPropertyChanged(BR.isSnap);
    }

    public void setFadeIn(boolean fadeIn) {
        this.isFadeIn = fadeIn;
        notifyPropertyChanged(BR.isFadeIn);
    }

    public void setTheme(int theme) {
        this.theme = theme;
        notifyPropertyChanged(BR.theme);
    }

    public void setRingtoneName(String ringtoneName) {
        this.ringtoneName = ringtoneName;
        notifyPropertyChanged(BR.ringtoneName);
    }

    public void updateState() {
        State state = ImmutableState.builder()
                .alarm(ImmutableAlarm.builder()
                        .alarmTime(QuickAlarmApp.getState().alarm().alarmTime())
                        .build())
                .settings(ImmutableSettings.builder()
                        .ringtone(QuickAlarmApp.getState().settings().ringtone())
                        .ramping(isFadeIn)
                        .snap(isSnap)
                        .vibrate(isVibrate)
                        .theme(theme)
                        .snoozeDuration(QuickAlarmApp.getState().settings().snoozeDuration())
                        .build())
                .build();

        QuickAlarmApp.setState(state);
    }

}