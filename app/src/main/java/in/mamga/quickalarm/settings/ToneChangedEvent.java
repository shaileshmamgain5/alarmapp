package in.mamga.quickalarm.settings;

/**
 * Created by shailesh on 13/06/17.
 */

public class ToneChangedEvent {
    public final String newName;
    public ToneChangedEvent(String newName) {
        this.newName = newName;
    }
}
