package in.mamga.quickalarm.settings;

import android.media.RingtoneManager;

import org.immutables.gson.Gson;
import org.immutables.value.Value;

@Value.Immutable
@Gson.TypeAdapters
public interface State {

    Settings settings();

    Alarm alarm();

    @Value.Immutable
    interface Settings {
        boolean vibrate();

        boolean snap();

        boolean ramping();

        String ringtone();

        int snoozeDuration();

        int theme();
    }

    @Value.Immutable
    abstract class Alarm {
        public abstract long alarmTime();
    }

    class Default {
        public static State build() {
            return ImmutableState.builder()
                    .alarm(ImmutableAlarm.builder()
                            .alarmTime(System.currentTimeMillis())
                            .build())
                    .settings(ImmutableSettings.builder()
                            .ringtone(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString())
                            .ramping(true)
                            .snap(true)
                            .vibrate(true)
                            .theme(0)
                            .snoozeDuration(1)
                            .build())
                    .build();
        }
    }

}
