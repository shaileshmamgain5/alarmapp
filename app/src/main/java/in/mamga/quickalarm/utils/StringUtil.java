package in.mamga.quickalarm.utils;

/**
 * Created by photography on 17/03/17.
 */

public class StringUtil {
    public static String CURRENT_CITY_ID_KEY = "CURRENT_CITY_ID_KEY";
    public static final int NUMBER_OF_DAY_FORECAST = 7;


    public static final String SNOOZE_LAST_STARTED_KEY = "SNOOZE_LAST_STARTED_KEY";
    public static final String SNOOZE_IS_ON_KEY = "SNOOZE_IS_ON_KEY";
    public static final String NAP_ALARMS_INITIALIZED_KEY = "NAP_ALARMS_INITIALIZED_KEY";
}
