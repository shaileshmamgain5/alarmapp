package in.mamga.quickalarm.utils;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.graphics.Palette;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import in.mamga.quickalarm.database.AlarmItem;


/**
 * Created by Shailesh on 04/03/17.
 */

public class Util {
    public static StringBuilder getURLParams(Map<String, String> parameter) {
        StringBuilder stringBuilder = new StringBuilder("?");
        for (Map.Entry<String, String> entry : parameter.entrySet())
            try {
                stringBuilder.append(entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8") + "&");
            } catch (UnsupportedEncodingException e) {
                //Thrown when URLEncoder is provided with a value that cannot be encoded
                e.printStackTrace();
                stringBuilder.append(entry.getKey() + "=" + entry.getValue() + "&");
            } catch (NullPointerException e) {
                //Thrown when An entry has null value
                e.printStackTrace();
                stringBuilder.append(entry.getKey() + "=" + entry.getValue() + "&");
            }
        return stringBuilder;
    }

    public static void setStringArrayPref(Context context, String key, ArrayList<String> values) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        JSONArray a = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            a.put(values.get(i));
        }
        if (!values.isEmpty()) {
            editor.putString(key, a.toString());
        } else {
            editor.putString(key, null);
        }
        editor.commit();
    }

    public static ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static boolean checkPermission(final Context context) {
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }


    /**
     * Displays local time.
     *
     * @return
     */
    public static String getLocalDisplayTimeInString() {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR);
        String h = hour < 10 ? "0" + hour : "" + hour;
        int min = cal.get(Calendar.MINUTE);
        String m = min < 10 ? "0" + min : "" + min;
        String time = h + ":" + m + " "
                + ((cal.get(Calendar.HOUR_OF_DAY) >= 12) ? "pm" : "am");

        return time;
    }

    public static float getTempInCelsius(double temp) {
        return (float) (temp - 273.15d);

    }

    public static float getTempInFarenhite(double temp) {
        return (float) ((9d / 5) * (temp - 273.15d) + 32d);
    }

    public static Long calculateNextAlarm(AlarmItem item) {
        Long nextAlarm = -1l;
        Date today = new Date(System.currentTimeMillis());

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(today);
        cal2.set(Calendar.HOUR, item.getHour());
        cal2.set(Calendar.MINUTE, item.getMin());
        cal2.set(Calendar.SECOND,0);
        cal2.set(Calendar.AM_PM, item.getIsAm() ? Calendar.AM : Calendar.PM);
        long time = cal2.getTimeInMillis();
        if (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) && (time > (System.currentTimeMillis() + 1000l))) {
            if (!item.getIsRepeat()) {
                nextAlarm = time;
            } else {
                int dayOfWeek = cal2.get(Calendar.DAY_OF_WEEK);
                switch (dayOfWeek) {
                    case 1:
                        if (item.getIsSun()) {
                            nextAlarm = time;
                        }
                        break;
                    case 2: if (item.getIsMon()) {
                        nextAlarm = time;
                    }
                        break;
                    case 3:
                        if (item.getIsTue()) {
                            nextAlarm = time;
                        }
                        break;
                    case 4:
                        if (item.getIsWed()) {
                            nextAlarm = time;
                        }
                        break;
                    case 5:
                        if (item.getIsThu()) {
                            nextAlarm = time;
                        }
                        break;
                    case 6:
                        if (item.getIsFri()) {
                            nextAlarm = time;
                        }
                        break;
                    case 7:
                        if (item.getIsSat()) {
                            nextAlarm = time;
                        }
                        break;
                }
            }
        } else {
            cal2.add(Calendar.DAY_OF_YEAR,1);
            if (item.getIsRepeat()) {
                int target = -1;
                int nextDay = cal2.get(Calendar.DAY_OF_WEEK);
                //first we try from that day to end of week
                target = findNextRepeat(item, cal2.get(Calendar.DAY_OF_WEEK));
                if (target == 0) {
                    //if not after today, we'll add few days
                    target = findNextRepeat(item, 1);
                }
                if (target == 0) {
                    item.setIsRepeat(false);
                } else {
                    if (target >= nextDay) {
                        cal2.add(Calendar.DAY_OF_YEAR,(target - nextDay));
                    } else {
                        cal2.add(Calendar.DAY_OF_YEAR, (target + (7 - nextDay)));
                    }
                    nextAlarm = cal2.getTimeInMillis();
                }

            }
            if (!item.getIsRepeat()) {
                nextAlarm = cal2.getTimeInMillis();
            }
        }

        Log.i("NextAlarm", cal2.get(Calendar.DAY_OF_WEEK) + "  " + cal2.get(Calendar.DAY_OF_MONTH));
        return nextAlarm;

    }

    public static int findNextRepeat(AlarmItem item, int startDay) {
        int next = 0;
        switch (startDay) {
            case 1:
                if (item.getIsSun()) {
                    next = 1;
                    break;
                }
            case 2:
                if (item.getIsMon()) {
                    next = 2;
                    break;
                }
            case 3:
                if (item.getIsTue()) {
                    next = 3;
                    break;
                }
            case 4:
                if (item.getIsWed()) {
                    next = 4;
                    break;
                }
            case 5:
                if (item.getIsThu()) {
                    next = 5;
                    break;
                }
            case 6:
                if (item.getIsFri()) {
                    next = 6;
                    break;
                }
            case 7:
                if (item.getIsSat()) {
                    next = 7;
                }
                break;
            default:
                item.setIsRepeat(false);
        }
        return next;
    }


    public static String getTimeRemaining(AlarmItem alarmItem) {
        StringBuilder time = new StringBuilder();
        time.append("");
        long ms = alarmItem.getNextAlarm() - System.currentTimeMillis();
        if (alarmItem != null && ms > 0) {
            //first clear case of less than a minute
            if (ms < 60*1000) {
                return " after less then a minute";
            }

            time.append( " after");
            int days = (int)(ms/(3600 * 24 * 1000));
            if (days > 0) {
                if (days == 1) {
                    time.append(" 1 day");
                } else {
                    time.append(" " + days + " days");
                }
            }
            int hours = (int)((ms - (days*(3600 * 24 * 1000))) / (3600 * 1000) ) ;
            if (hours > 0) {
                if (days > 0) {
                    time.append(",");
                }
                if (hours == 1) {
                    time.append(" 1 hr");
                } else {
                    time.append(" " + hours + " hrs");
                }
            }
            int min = (int)((ms - (days*(3600 * 24 * 1000) + hours *(3600 * 1000))) / (60 * 1000) ) ;
            if (min > 0) {
                if (days > 0 || hours > 0) {
                    time.append(" and");
                }
                if (min == 1) {
                    time.append(" 1 min");
                } else {
                    time.append(" " + min + " mins");
                }
            }
        }
        return time.toString();
    }

    public static int getSecondsInBetween(long past, long present) {
        return (int)((present - past) / (1000));
    }


    public static int getDominantColor(Bitmap bitmap) {
        List<Palette.Swatch> swatchesTemp = Palette.from(bitmap).generate().getSwatches();
        List<Palette.Swatch> swatches = new ArrayList<Palette.Swatch>(swatchesTemp);
        Collections.sort(swatches, new Comparator<Palette.Swatch>() {
            @Override
            public int compare(Palette.Swatch swatch1, Palette.Swatch swatch2) {
                return swatch2.getPopulation() - swatch1.getPopulation();
            }
        });
        return swatches.size() > 0 ? swatches.get(0).getRgb() : -1;
    }
}
