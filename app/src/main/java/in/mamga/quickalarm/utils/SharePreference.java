package in.mamga.quickalarm.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Shailesh on 10/11/2014.
 */
public class SharePreference {
    public static String preferenceName = "ChimesPreferences";
    private static SharePreference sharePreference;
    public SharedPreferences preference;
    public SharedPreferences.Editor editor;

    private SharePreference(Context ctx) {
        preference = ctx.getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = preference.edit();

    }

    public static  SharePreference getInstance(Context context) {
        if (sharePreference == null)
        {
            sharePreference = new SharePreference(context);

        }
        return sharePreference;
    }

    public void putString(String key, String value) {
        if (editor != null) {
            editor.putString(key, value);
            editor.commit();
        }
    }

    public void putInt(String key, int value) {
        if (editor != null) {
            editor.putInt(key, value);
            editor.commit();
        }
    }

    public void putLong(String key, long value) {
        if (editor != null) {
            editor.putLong(key, value);
            editor.commit();
        }
    }

    public void putFloat(String key, float value) {
        if (editor != null) {
            editor.putFloat(key, value);
            editor.commit();
        }
    }

    public void putBoolean(String key, boolean value) {
        if (editor != null) {
            editor.putBoolean(key, value);
            editor.commit();
        }
    }

    public boolean getBoolean(String key) {
        if (preference != null)
            return preference.getBoolean(key, false);
        else
            return false;


    }

    public String getString(String key) {
        if (preference != null)
            return preference.getString(key, "");
        else
            return "";


    }

    public int getInt(String key) {
        if (preference != null)
            return preference.getInt(key, 0);
        else
            return 0;


    }
    public Long getLong(String key) {
        if (preference != null)
            return preference.getLong(key, 0);
        else
            return 0L;


    }

    public Float getFloat(String key) {
        if (preference != null)
            return preference.getFloat(key, 0);
        else
            return 0F;


    }

}