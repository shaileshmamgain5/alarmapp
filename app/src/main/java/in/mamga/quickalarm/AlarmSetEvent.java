package in.mamga.quickalarm;

import com.google.android.gms.maps.model.LatLng;

/**
 * Fires when user has selectd a city from add a city
 */

public class AlarmSetEvent {
    public final long timeInMillis;
    public AlarmSetEvent(long timeInMillis) {
        this.timeInMillis = timeInMillis;
    }
}
