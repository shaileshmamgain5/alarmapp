package in.mamga.quickalarm;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DaoMaster;
import in.mamga.quickalarm.database.DaoSession;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.settings.ImmutableAlarm;
import in.mamga.quickalarm.settings.ImmutableSettings;
import in.mamga.quickalarm.settings.ImmutableState;
import in.mamga.quickalarm.settings.PersistanceController;
import in.mamga.quickalarm.settings.State;

/**
 * Created by Mahima on 17/03/17.
 */

public class QuickAlarmApp extends Application {

    public static SQLiteDatabase db;

    public static final String DATABASE_NAME = "WeatherAndTimeDb";

    private static DaoMaster daoMaster;
    private static DaoMaster.DevOpenHelper helper;
    private static DaoSession daoSession;


    private static State appState = null;
    private static PersistanceController persistanceController;

    private static QuickAlarmApp instance;

    public static boolean isAlarmActivityVisible = false;

    //We keep a copy of current alarm that is se by user, so that its progress can be preserved
    private static AlarmItem currentAlarmItem;

    public void onCreate() {
        super.onCreate();
        persistanceController = new PersistanceController(this);
        appState = persistanceController.getSavedState();
        instance = this;

        initGreenDAO();
    }

    private void initGreenDAO() {
        helper = new DaoMaster.DevOpenHelper(QuickAlarmApp.this, DATABASE_NAME, null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public static DaoSession getDaoSession() {
        if (daoSession != null) {
            return daoSession;
        }
        return daoSession;
    }

    public static State getState() {
        if (appState == null) {
            appState = State.Default.build();
        }
        return appState;
    }

    public static void setState(State state) {
        if (state != null) {
            appState = state;
            persistanceController.updateState(state);
        }
    }

    public static AlarmItem getCurrentAlarmItem() {
        if (currentAlarmItem == null) {
            currentAlarmItem = new AlarmItem();
        }
        return currentAlarmItem;
    }

    public static void setCurrentAlarmItem(AlarmItem alarmItem) {
        if (alarmItem == null) {
            currentAlarmItem = new AlarmItem();
        } else {
            currentAlarmItem = alarmItem;
        }
    }

    public static void updateAlarmTime(long time) {
        State state = ImmutableState.builder()
                .alarm(ImmutableAlarm.builder()
                        .alarmTime(time)
                        .build())
                .settings(ImmutableSettings.builder()
                        .ringtone(getState().settings().ringtone())
                        .ramping(getState().settings().ramping())
                        .snap(getState().settings().snap())
                        .vibrate(getState().settings().vibrate())
                        .theme(getState().settings().theme())
                        .snoozeDuration(getState().settings().snoozeDuration())
                        .build())
                .build();

        setState(state);
    }

    public static void updateRingTone(String uri) {
        State state = ImmutableState.builder()
                .alarm(ImmutableAlarm.builder()
                        .alarmTime(getState().alarm().alarmTime())
                        .build())
                .settings(ImmutableSettings.builder()
                        .ringtone(uri)
                        .ramping(getState().settings().ramping())
                        .snap(getState().settings().snap())
                        .vibrate(getState().settings().vibrate())
                        .theme(getState().settings().theme())
                        .snoozeDuration(getState().settings().snoozeDuration())
                        .build())
                .build();

        setState(state);

    }
}
