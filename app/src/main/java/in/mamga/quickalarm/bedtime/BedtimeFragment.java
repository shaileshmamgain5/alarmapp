package in.mamga.quickalarm.bedtime;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import in.mamga.quickalarm.R;
import in.mamga.quickalarm.ui.circlebedtime.CircleAlarmTimerView;

/**
 * Created by Shailesh on 15/03/17.
 */

public class BedtimeFragment extends Fragment {
    public static final String TAG = "ActiveAlarmsFragment";

    private TextView textView1;
    private TextView textView2;
    private CircleAlarmTimerView circleAlarmTimerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bedtime, parent, false);

       /* FragmentActiveAlarmsBinding activeAlarmsBinding = DataBindingUtil.bind(view);

        activeAlarmsViewModel = new ActiveAlarmsViewModel();

        Bundle bundle = getArguments();
        if (bundle != null) {

        }
        activeAlarmsBinding.setActiveAlarmsViewModel(activeAlarmsViewModel);
        */

       initView(view);
        return view;
    }
    private void initView(View parent){
        textView1 = (TextView) parent.findViewById(R.id.start);
        textView2 = (TextView) parent.findViewById(R.id.end);

        circleAlarmTimerView = (CircleAlarmTimerView) parent.findViewById(R.id.circletimerview);
        circleAlarmTimerView.setOnTimeChangedListener(new CircleAlarmTimerView.OnTimeChangedListener() {
            @Override
            public void start(String starting) {
                textView1.setText(starting);
            }

            @Override
            public void end(String ending) {
                textView2.setText(ending);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        circleAlarmTimerView.initBedTime(new ClockTime(9, 30, false));
    }

    @Override
    public void onStop() {
        super.onStop();
        //activeAlarmsViewModel.onStop(TAG);
    }

}