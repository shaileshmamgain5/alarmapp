package in.mamga.quickalarm.bedtime;

/**
 * Created by shailesh on 14/09/17.
 */

public class ClockTime {
    private int hours;
    private int mins;
    private  boolean isAm;

    public ClockTime(int hours, int mins, boolean isAm) {
        this.hours = hours;
        this.mins = mins;
        this.isAm = isAm;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMins() {
        return mins;
    }

    public void setMins(int mins) {
        this.mins = mins;
    }

    public boolean isAm() {
        return isAm;
    }

    public void setAm(boolean am) {
        isAm = am;
    }
}
