package in.mamga.quickalarm.napalarm;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;
import in.mamga.quickalarm.R;
import in.mamga.quickalarm.napalarm.events.NapTimerEvent;
import me.itangqi.waveloadingview.WaveLoadingView;

/**
 * Created by shailesh on 19/08/17.
 */

public class NapAlarmProgressActivity extends AppCompatActivity implements NapAlarmService.ServiceCallBacks {
    public static final String TAG = NapAlarmProgressActivity.class.getSimpleName();
    WaveLoadingView timerWave;

    NapAlarmService myService;
    BlurView blurView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nap_progress);
        timerWave = (WaveLoadingView) findViewById(R.id.timerWave);
        blurView = (BlurView) findViewById(R.id.blurView);

        final float radius = 20;

        final View decorView = getWindow().getDecorView();
        //Activity's root View. Can also be root View of your layout (preferably)
        final ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        //set background, if your root layout doesn't have one
        final Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .windowBackground(windowBackground)
                .blurAlgorithm(new RenderScriptBlur(this))
                .blurRadius(radius);

        //Intent intent = new Intent(this, NapAlarmService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void napAlarmTicked(long data, long total) {
        Toast.makeText(this, "tick", Toast.LENGTH_LONG).show();
        int progress = (int) (((float)data / (float)total) * (float)100);
        timerWave.setProgressValue(progress);
    }

    @Override
    public void napAlarmTimerFinished(long data) {

    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            Toast.makeText(NapAlarmProgressActivity.this, "onServiceConnected called", Toast.LENGTH_SHORT).show();
            // We've binded to LocalService, cast the IBinder and get LocalService instance
            NapAlarmService.LocalBinder binder = (NapAlarmService.LocalBinder) service;
            myService = binder.getServiceInstance(); //Get instance of your service!
            myService.registerClient(NapAlarmProgressActivity.this); //Activity register in the service as client for callabcks!
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Toast.makeText(NapAlarmProgressActivity.this, "onServiceDisconnected called", Toast.LENGTH_SHORT).show();
            //Finish activity as well as it doesn't make sense here
            finish();
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNapTimerEvent(NapTimerEvent event) {
        if (event.isFinished) {
            return;
        }
        //Toast.makeText(this, "tick", Toast.LENGTH_LONG).show();
         int progress =(int) (((float)event.progress/(float)event.total) * (float)100);
         timerWave.setProgressValue(progress);
    }

}
