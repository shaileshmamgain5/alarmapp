package in.mamga.quickalarm.napalarm;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.R;
import in.mamga.quickalarm.alarmhandlers.AlarmActivity;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.napalarm.events.NapTimerEvent;

import static in.mamga.quickalarm.napalarm.NapConstants.KEY_ACTIVE_NAP_ALARM_ID;


/**
 * Created by shailesh on 05/08/17.
 */

public class NapAlarmService extends Service {
    public static final String TAG = NapAlarmService.class.getSimpleName();
    public static boolean isNapServiceRunning = false;

    // Time period between two vibration events
    private final static int VIBRATE_DELAY_TIME = 2000;
    // Vibrate for 1000 milliseconds
    private final static int DURATION_OF_VIBRATION = 100;
    // Increase alarm volume gradually every 600ms
    private final static int VOLUME_INCREASE_DELAY = 600;
    // Volume level increasing step
    private final static float VOLUME_INCREASE_STEP = 0.01f;
    // Max player volume level
    private final static float NUDGE_VOLUME = 0.6f;

    NapAlarmItem activeNapAlarm;

    private MediaPlayer mPlayer;
    private Vibrator mVibrator;
    private float mVolumeLevel = 0;

    int timeRemaining = -1;
    int duration;

    private Handler mHandler = new Handler();

    NotificationManager notificationManager;
    NotificationCompat.Builder mBuilder;
    ServiceCallBacks activity;
    private long startTime = 0;
    private long millis = 0;
    private final IBinder mBinder = new LocalBinder();
    Handler handler = new Handler();


    private Runnable mVibrationRunnable = new Runnable() {
        @Override
        public void run() {
            if (mVibrator == null) {
                return;
            }
            mVibrator.vibrate(DURATION_OF_VIBRATION);
            // Provide loop for vibration
            mHandler.postDelayed(mVibrationRunnable,
                    DURATION_OF_VIBRATION + VIBRATE_DELAY_TIME);
        }
    };

    private Runnable mVolumeRunnable = new Runnable() {
        @Override
        public void run() {
            // increase volume level until reach max value
            if (mPlayer != null && mVolumeLevel < NUDGE_VOLUME) {
                mVolumeLevel += VOLUME_INCREASE_STEP;
                mPlayer.setVolume(mVolumeLevel, mVolumeLevel);
                // set next increase in 600ms
                mHandler.postDelayed(mVolumeRunnable, VOLUME_INCREASE_DELAY);
            }
        }
    };


    private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            mp.stop();
            mp.release();
            mHandler.removeCallbacksAndMessages(null);
            NapAlarmService.this.stopSelf();
            return true;
        }
    };

    @Override
    public void onCreate() {
        HandlerThread ht = new HandlerThread("alarm_service");
        ht.start();
        mHandler = new Handler(ht.getLooper());
        //Toggling state
        isNapServiceRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        if (bundle != null && bundle.containsKey(KEY_ACTIVE_NAP_ALARM_ID)) {
            Long alarmId = bundle.getLong(KEY_ACTIVE_NAP_ALARM_ID);

            NapAlarmItem alarmToStart = DbUtil.getNapAlarmById(alarmId);

            //what if some alarm is already active
            if (activeNapAlarm != null) {
                if (activeNapAlarm.getDuration() != alarmToStart.getDuration()) {
                    //Making it not active when its turned off
                    NapManager.updateIsActiveInDb(activeNapAlarm, false);
                    activeNapAlarm = null;
                }
            }
            activeNapAlarm = alarmToStart;
            if (activeNapAlarm != null) {
                duration = activeNapAlarm.getDuration();
                startNapTimer(activeNapAlarm);
            }
        }
        //handling the pending intent that might come from notification
        handleIntentActions(intent);
        // Start the activity where you can stop alarm
        //startAlarmActivity();

        return START_STICKY;
    }

    private void handleIntentActions(Intent intent) {
        if (intent.getAction() == null) {
            return;
        }
        if (intent.getAction().equalsIgnoreCase(NapConstants.MINUS_TEN_PERCENT_ACTION)) {
            Log.i(TAG, "Clicked Previous");

        } else if (intent.getAction().equalsIgnoreCase(NapConstants.ADD_TEN_PERCENT_ACTION)) {
            Log.i(TAG, "Clicked next");
        } else if (intent.getAction().equalsIgnoreCase(NapConstants.DISMISS_NAP_ACTION)) {
            Log.i(TAG, "Clicked Dismiss");
        } else if (intent.getAction().equalsIgnoreCase(NapConstants.NAP_NOTIFICATION_INTENT)) {
            Log.i(TAG, "Received nap notification.");
            stopForeground(true);
            stopSelf();
        }
    }


    private void startNapTimer(final NapAlarmItem napAlarmItem) {
        Intent napProgress = new Intent(this, NapAlarmProgressActivity.class);
        napProgress.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        startActivity(napProgress);
        if (ctTimer != null) {
            //meaning some timer is already running
            ctTimer.cancel();
        }
        ctTimer = new CountDownTimer((duration * 60 * 1000), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                onNapTimerTick(millisUntilFinished);
            }

            @Override
            public void onFinish() {
                onNapTimerFinish();
            }
        };
        ctTimer.start();
        startNotification();
    }

    private void onNapTimerFinish() {

    }

    private void onNapTimerTick(long timeElapsed) {
        if (activity != null) {
            // activity.napAlarmTicked(timeElapsed, (activeNapAlarm.getDuration()*1000*60));
        }
        long totalTime = (duration * 1000 * 60);
        //Also updatting the notificaition
        timeRemaining = (int) ((timeElapsed) / 60000f);
        startNotification();
        EventBus.getDefault().post(new NapTimerEvent(timeElapsed, totalTime, false));

    }

    private void startAlarmActivity() {
        //if (!SharePreference.getInstance(this).getBoolean(Constants.IS_ALARM_ACTIVITY_ON)) {
        // Start the activity where you can stop alarm
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.setComponent(new ComponentName(this, AlarmActivity.class));
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
        // }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
        }
        if (ctTimer != null)
            ctTimer.cancel();
        mHandler.removeCallbacksAndMessages(null);
        isNapServiceRunning = false;
    }

    private void startPlayer() {
        mPlayer = new MediaPlayer();
        mPlayer.setOnErrorListener(mErrorListener);

        try {
            // add vibration to alarm alert if it is set
            // if (App.getState().settings().vibrate()) {
            if (QuickAlarmApp.getState().settings().vibrate()) {
                mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                mHandler.post(mVibrationRunnable);
            }
            // Player setup is here
            String ringtone = QuickAlarmApp.getState().settings().ringtone();
            if (TextUtils.isEmpty(ringtone)) {
                ringtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && ringtone.startsWith("content://media/external/")
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                ringtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString();
            }
            mPlayer.setDataSource(this, Uri.parse(ringtone));
            mPlayer.setLooping(true);
            mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
            mPlayer.setVolume(mVolumeLevel, mVolumeLevel);
            mPlayer.prepare();
            mPlayer.start();

            //if (App.getState().settings().ramping()) {
            if (QuickAlarmApp.getState().settings().ramping()) {
                mHandler.postDelayed(mVolumeRunnable, VOLUME_INCREASE_DELAY);
            } else {
                mPlayer.setVolume(NUDGE_VOLUME, NUDGE_VOLUME);
            }
        } catch (Exception e) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            ctTimer.cancel();
            stopSelf();
        }
    }

    CountDownTimer ctTimer;

    private void playNudge() {
        startPlayer();
    }

    //Here Activity register to the service as Callbacks client
    public void registerClient(Activity activity) {
        this.activity = (ServiceCallBacks) activity;
    }

    public String getTotalAndRemainingString() {
        if (duration <= 0 || timeRemaining < 0) {
            return "Timer in progress!";
        }

        StringBuilder s = new StringBuilder();
        s.append(getHoursString(timeRemaining, duration));
        s.append(getMinString(timeRemaining, duration));
        s.append(" remaining of ");
        s.append(getHoursString(duration, duration));
        s.append(getMinString(duration, duration));
        return s.toString();
    }

    private String getHoursString(int duration, int total) {
        if (total < 60) {
            return "";
        } else {
            int h = (int) (duration / 60f);
            if (h < 10) {
                return "0" + h + "h : ";
            } else {
                return h + "h : ";
            }
        }
    }

    private String getMinString(int duration, int total) {
        int m = (int) (duration % 60f);
        if (m < 10) {
            return "0" + m + "m";
        } else {
            return m + "m";
        }

    }

    //callbacks interface for communication with service clients!
    public interface ServiceCallBacks {
        public void napAlarmTicked(long data, long total);

        public void napAlarmTimerFinished(long data);
    }

    //returns the instance of the service
    public class LocalBinder extends Binder {
        public NapAlarmService getServiceInstance() {
            return NapAlarmService.this;
        }
    }

    public void startNotification() {
        String description = getTotalAndRemainingString();
        int toAdd = getPercentOfDuration(10);
        Intent notificationIntent = new Intent(this, NapAlarmService.class);
        notificationIntent.setAction(NapConstants.NAP_NOTIFICATION_INTENT);
        /*notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
        PendingIntent pnotificationIntent = PendingIntent.getService(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent minTenIntent = new Intent(this, NapAlarmService.class);
        minTenIntent.setAction(NapConstants.MINUS_TEN_PERCENT_ACTION);
        PendingIntent pminTenIntent = PendingIntent.getService(this, 0,
                minTenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent dismissIntent = new Intent(this, NapAlarmService.class);
        dismissIntent.setAction(NapConstants.DISMISS_NAP_ACTION);
        PendingIntent pdismissIntent = PendingIntent.getService(this, 0,
                dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent addTenIntent = new Intent(this, NapAlarmService.class);
        addTenIntent.setAction(NapConstants.ADD_TEN_PERCENT_ACTION);
        PendingIntent paddTenIntent = PendingIntent.getService(this, 0,
                addTenIntent, PendingIntent.FLAG_UPDATE_CURRENT);

       /* Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.truiton_short);*/
        Notification notification = new NotificationCompat.Builder(this)

                .setContentTitle("Nap active")
                .setTicker("Nap active")
                .setContentText(description)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pnotificationIntent)
                .setOngoing(true)
                .addAction(-1, "Reduce " + toAdd + " min", pminTenIntent)
                .addAction(-1, "Add " + toAdd + " min", paddTenIntent)
                .addAction(-1, "Dismiss alarm", pdismissIntent).build();
        startForeground(NapConstants.NAP_NOTIFICATION_ID,
                notification);
    }

    private int getPercentOfDuration(int percent) {
        int dur = activeNapAlarm.getDuration();
        if (dur < 60) {
            return 5;
        } else if (dur < 120) {
            return 10;
        } else if (dur < 360) {
            return 15;
        } else {
            return 20;
        }
    }

}
