package in.mamga.quickalarm.napalarm;

/**
 * Created by shailesh on 19/08/17.
 */

public class NapConstants {
    public static final String KEY_ACTIVE_NAP_ALARM_ID = "KEY_ACTIVE_NAP_ALARM_ID";

    public static String DISMISS_NAP_ACTION = "in.mamga.quickalarm.napalarm.dismissnap";
    public static String ADD_TEN_PERCENT_ACTION = "in.mamga.quickalarm.napalarm.addten";
    public static String MINUS_TEN_PERCENT_ACTION = "in.mamga.quickalarm.napalarm.minusten";
    public static String NAP_NOTIFICATION_INTENT = "in.mamga.quickalarm.napalarm.notificationnap";
    public static int NAP_NOTIFICATION_ID = 55555;
    public static String NEXT_ACTION = "com.truiton.foregroundservice.action.next";
    public static String STARTFOREGROUND_ACTION = "com.truiton.foregroundservice.action.startforeground";
    public static String STOPFOREGROUND_ACTION = "com.truiton.foregroundservice.action.stopforeground";
}
