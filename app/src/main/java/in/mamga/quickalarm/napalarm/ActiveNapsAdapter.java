package in.mamga.quickalarm.napalarm;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kyleduo.switchbutton.SwitchButton;

import java.util.List;

import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.databinding.ItemNapAlarmBinding;

/**
 * Created by shailesh on 12/08/17.
 */

public class ActiveNapsAdapter extends RecyclerView.Adapter<ActiveNapsAdapter.NapsItemViewHolder> {
    List<NapAlarmItem> napAlarmItems;

    public ActiveNapsAdapter(List<NapAlarmItem> napAlarmItems) {
        this.napAlarmItems = napAlarmItems;
    }

    @Override
    public NapsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(parent.getContext());
        ItemNapAlarmBinding itemAlarmBinding =
                ItemNapAlarmBinding.inflate(inflator, parent, false);
        return new NapsItemViewHolder(itemAlarmBinding);
    }

    @Override
    public void onBindViewHolder(NapsItemViewHolder holder, int position) {
        holder.bind(napAlarmItems.get(position), position);
        Log.i("Adapter", "Layout inflated == " + position);
    }

    @Override
    public int getItemCount() {
        return napAlarmItems.size();
    }

    public class NapsItemViewHolder extends RecyclerView.ViewHolder {

        private final ItemNapAlarmBinding itemAlarmBinding;
        SwitchButton sbActive;

        public NapsItemViewHolder(ItemNapAlarmBinding itemAlarmBinding) {
            super(itemAlarmBinding.getRoot());
            this.itemAlarmBinding = itemAlarmBinding;
        }

        public void bind(final NapAlarmItem alarmItem, final int position) {
            final NapAlarmItemViewModel viewModel = new NapAlarmItemViewModel(alarmItem);
            itemAlarmBinding.setNapItemViewModel(viewModel);
            itemAlarmBinding.waveLoadingView.setCenterTitle(viewModel.timeInString);
            //setOnClickListeners

            itemAlarmBinding.executePendingBindings();
        }
    }
}
