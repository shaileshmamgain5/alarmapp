package in.mamga.quickalarm.napalarm;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.napalarm.events.NapAlarmDeletedEvent;

/**
 * Created by shailesh on 15/08/17.
 */

public class NapAlarmDetailItemViewModel extends BaseObservable {

    public static final String TAG = NapAlarmDetailItemViewModel.class.getSimpleName();
    public static final int DEFAULT_TIME = 10; //in min

    @Bindable
    public String timeInString;

    @Bindable
    public boolean isNotification;

    @Bindable
    public boolean isHeadphonesOnly;

    NapAlarmItem napAlarmItem;

    public NapAlarmDetailItemViewModel(NapAlarmItem item) {
        if (item == null) {
            item = new NapAlarmItem();
            item.setDuration(10);
        }
        this.napAlarmItem = item;
        setUpViewModel(item);
    }

    public void setUpViewModel(NapAlarmItem upViewModel) {
        setHeadphonesOnly(upViewModel.getIsHeadphoneOnly());
        setNotification(upViewModel.getInNotification());
        updateTimeString(upViewModel.getDuration());
    }

    public void updateTimeString(int duration) {
        //first update the duration in item
        napAlarmItem.setDuration(duration);

        String dur = "";
        String mS = "";
        String hS = "";
        int h = duration / 60;
        int min = duration % 60;
        if (h < 10) {
            hS = "0" + h;
         } else {
            hS = h + "";
        }
        if (min < 10) {
            mS = "0" + min;
        } else {
            mS = min + "";
        }
        dur = hS + " : " + mS;

        Log.i(TAG, "timestring = " + dur);
        setTimeInString(dur);
    }

    public void setTimeInString(String timeInString) {
        this.timeInString = timeInString;
        notifyPropertyChanged(BR.timeInString);
    }

    public void setNotification(boolean notification) {
        isNotification = notification;
        notifyPropertyChanged(BR.isNotification);
    }

    public void setHeadphonesOnly(boolean headphonesOnly) {
        isHeadphonesOnly = headphonesOnly;
        notifyPropertyChanged(BR.isHeadphonesOnly);
    }


    public void saveAndStartAlarm(NapAlarmItem alarmItem) {
        alarmItem.setIsActive(true);
        //saveAlarm(alarmItem);
        //Todo : startAlarm here
    }

    public void onEmptyRegionclicked(View view) {

    }
    public void deleteAlarmItem() {
        DbUtil.deleteNapAlarm(napAlarmItem);
        EventBus.getDefault().post(new NapAlarmDeletedEvent());
    }
}
