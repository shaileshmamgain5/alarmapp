package in.mamga.quickalarm.napalarm;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.napalarm.events.NapAlarmStartedEvent;

/**
 * Created by shailesh on 06/08/17.
 */

public class NapAlarmItemViewModel extends BaseObservable {
    public static final String TAG  = NapAlarmItemViewModel.class.getSimpleName();

    NapAlarmItem napAlarmItem;
    @Bindable
    public String timeInString;

    public NapAlarmItemViewModel(@NonNull NapAlarmItem napAlarmItem) {
        this.napAlarmItem = napAlarmItem;
        setUpViewModel(napAlarmItem);

    }

    public void setUpViewModel(NapAlarmItem upViewModel) {
        updateTimeString(upViewModel.getDuration());
    }

    /**
     * Here we consider progress from 0 to 88
     *
     * @param progress
     */
    public int getDurationFromProgress(int progress) {
        int duration   = -1;
        if (progress < 0 || progress > 88) {
            throw new ArrayIndexOutOfBoundsException();
        }
        progress = progress + 1;
        if (progress <= 25) {
            duration = progress;
        }else  if (progress > 25 && progress <= 50) {
            duration = 25 + (progress - 25)*5;
        }else  if (progress > 50 && progress <= 77) {
            duration = 150 + (progress - 50)*10;
        }else  if (progress > 77) {
            duration = 420 + (progress - 77)*15;
        }
        Log.i(TAG, "progress = " + progress + "  duration = " + duration);
        return duration; //in min
    }

    public void updateTimeString (int duration) {

        //first update the duration in item
        napAlarmItem.setDuration(duration);

        String dur = "";
        if (duration < 10) {
            dur = "0" + duration + " min";
        }else if (duration < 60) {
            dur = duration + " min";
        } else {
            int h = duration / 60;
            int min = duration % 60;
            dur = h + "h";
            if ( h > 1) {
                dur = dur + "";
            }
            if (min > 0) {
                dur = dur + " " + min + "m";
            }
        }
        Log.i(TAG, "timestring = " + dur);
        setTimeInString(dur);
    }

    public void setTimeInString(String timeInString) {
        this.timeInString = timeInString;
        notifyPropertyChanged(BR.timeInString);
    }

    public void onItemClicked (View view) {
        EventBus.getDefault().post(new NapAlarmStartedEvent(napAlarmItem));
    }



}
