package in.mamga.quickalarm.napalarm.events;

/**
 * Created by shailesh on 19/08/17.
 */

public class NapTimerEvent {
    public final long progress;
    public final long total;
    public final boolean isFinished;

    public NapTimerEvent(long progress, long total, boolean isFinished) {
        this.progress = progress;
        this.total = total;
        this.isFinished = isFinished;
    }
}
