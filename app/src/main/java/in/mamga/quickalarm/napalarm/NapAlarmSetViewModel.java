package in.mamga.quickalarm.napalarm;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.napalarm.events.NapAlarmAddedEvent;

/**
 * Created by shailesh on 11/08/17.
 */

public class NapAlarmSetViewModel extends BaseObservable{

    public static final String TAG = NapAlarmSetViewModel.class.getSimpleName();
    public static final int DEFAULT_TIME = 10; //in min

    @Bindable
    public String timeInString;

    @Bindable
    public boolean isNotification;

    @Bindable
    public boolean isHeadphonesOnly;

    NapAlarmItem napAlarmItem;

    public NapAlarmSetViewModel(NapAlarmItem item) {
        if (item == null) {
            item    = new NapAlarmItem();
            item.setDuration(10);
        }
        this.napAlarmItem = item;
        setUpViewModel(item);
    }
    public void setUpViewModel(NapAlarmItem upViewModel) {
        setHeadphonesOnly(upViewModel.getIsHeadphoneOnly());
        setNotification(upViewModel.getInNotification());
        updateTimeString(upViewModel.getDuration());
    }

    public void handleProgressChange(int progress) {
        updateTimeString(getDurationFromProgress(progress));
    }

    /**
     * Here we consider progress from 0 to 88
     *
     * @param progress
     */
    public int getDurationFromProgress(int progress) {
        int duration   = -1;
        if (progress < 0 || progress > 88) {
            throw new ArrayIndexOutOfBoundsException();
        }
        progress = progress + 1;
        if (progress <= 25) {
            duration = progress;
        }else  if (progress > 25 && progress <= 50) {
            duration = 25 + (progress - 25)*5;
        }else  if (progress > 50 && progress <= 77) {
            duration = 150 + (progress - 50)*10;
        }else  if (progress > 77) {
            duration = 420 + (progress - 77)*15;
        }
        Log.i(TAG, "progress = " + progress + "  duration = " + duration);
        return duration; //in min
    }

    public void updateTimeString (int duration) {

        //first update the duration in item
        napAlarmItem.setDuration(duration);

        String dur = "";
        if (duration < 10) {
            dur = "0" + duration + " min";
        }else if (duration < 60) {
            dur = duration + " min";
        } else {
            int h = duration / 60;
            int min = duration % 60;
            dur = h + "h";
            if ( h > 1) {
                dur = dur + "";
            }
            if (min > 0) {
                dur = dur + " " + min + "m";
            }
        }
        Log.i(TAG, "timestring = " + dur);
        setTimeInString(dur);
    }

    public void setTimeInString(String timeInString) {
        this.timeInString = timeInString;
        notifyPropertyChanged(BR.timeInString);
    }

    public void setNotification(boolean notification) {
        isNotification = notification;
        notifyPropertyChanged(BR.isNotification);
    }

    public void setHeadphonesOnly(boolean headphonesOnly) {
        isHeadphonesOnly = headphonesOnly;
        notifyPropertyChanged(BR.isHeadphonesOnly);
    }

    public void onNapSaveClicked(View view) {
        saveAlarm(napAlarmItem);
    }

    public void onNapStartClicked(View view) {
        saveAndStartAlarm(napAlarmItem);
    }

    public void saveAlarm (NapAlarmItem alarmItem) {
        napAlarmItem.setId(System.currentTimeMillis());
        DbUtil.insertOrUpdateNapAlarm(alarmItem);
        napAlarmItem = new NapAlarmItem();
        napAlarmItem.setDuration(10);
        EventBus.getDefault().post(new NapAlarmAddedEvent());
    }

    public void saveAndStartAlarm (NapAlarmItem alarmItem) {
        alarmItem.setIsActive(true);
        saveAlarm(alarmItem);
        //Todo : startAlarm here
    }
}
