package in.mamga.quickalarm.napalarm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import in.mamga.quickalarm.R;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.databinding.ItemNapAlarmDetailBinding;

/**
 * Created by shailesh on 15/08/17.
 */

public class NapAlarmDetailFragment extends DialogFragment {

    public static final String TAG = NapAlarmDetailFragment.class.getSimpleName();
    public static final String KEY_NAP_ALARM_ITEM_ID = "KEY_NAP_ALARM_ITEM_ID";
    NapAlarmItem napAlarmItem = null;

    LinearLayout llNapDetails;
    NapAlarmDetailItemViewModel napAlarmDetailItemViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_nap_alarm_detail, container, false);
        llNapDetails = (LinearLayout) view.findViewById(R.id.llNapDetails);
        ItemNapAlarmDetailBinding itemNapAlarmDetailBinding = DataBindingUtil.bind(llNapDetails);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(KEY_NAP_ALARM_ITEM_ID)) {
            napAlarmItem = DbUtil.getNapAlarmById(bundle.getLong(KEY_NAP_ALARM_ITEM_ID));
        }
        napAlarmDetailItemViewModel = new NapAlarmDetailItemViewModel(napAlarmItem);
        itemNapAlarmDetailBinding.setNapItemVM(napAlarmDetailItemViewModel);

        itemNapAlarmDetailBinding.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                napAlarmDetailItemViewModel.deleteAlarmItem();
                getDialog().dismiss();
            }
        });

        itemNapAlarmDetailBinding.btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NapAlarmService.isNapServiceRunning) {
                    showAlertForAlreadyRunning();
                } else {
                    NapManager.startNapAlarm(v.getContext(), napAlarmItem);
                }
            }
        });

        return view;
    }

    private void showAlertForAlreadyRunning() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Add the buttons
        builder.setPositiveButton("Cancel current and start", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                NapManager.startNapAlarm(getActivity(), napAlarmItem);
                getDialog().dismiss();
            }
        });
        builder.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                getDialog().dismiss();
            }
        });

// 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("It looks like a nap alarm is currently running, you may though cancel it and start afresh!")
                .setTitle("Nap running..");

// 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //HAndling something on pressing back
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog, int keyCode, android.view.KeyEvent event) {

                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {

                }
                return false; // pass on to be processed as normal
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
