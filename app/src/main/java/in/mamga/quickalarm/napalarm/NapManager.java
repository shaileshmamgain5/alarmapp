package in.mamga.quickalarm.napalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import in.mamga.quickalarm.MainActivity;
import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.alarmhandlers.AlarmReceiver;
import in.mamga.quickalarm.alarmhandlers.AlarmService;
import in.mamga.quickalarm.alarmhandlers.SnoozeService;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.settings.State;

import static in.mamga.quickalarm.napalarm.NapConstants.KEY_ACTIVE_NAP_ALARM_ID;


public class NapManager {
    public static final String TAG = NapManager.class.getSimpleName();

    private final Context mContext;

    public NapManager(Context c) {
        mContext = c;
    }


    public static void restartAlarm(State state, Context mContext) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(state.alarm().alarmTime());
        if (c.getTimeInMillis() < System.currentTimeMillis()) {
            setNextAlarm(mContext);
        } else {
            Intent intent = new Intent(mContext, AlarmReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent, 0);

            AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {        // KITKAT and later
                    am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), sender);
                } else {
                    am.setExact(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), sender);
                }
                intent = new Intent("android.intent.action.ALARM_CHANGED");
                intent.putExtra("alarmSet", true);
                mContext.sendBroadcast(intent);
                SimpleDateFormat fmt = new SimpleDateFormat("E HH:mm");
                Settings.System.putString(mContext.getContentResolver(),
                        Settings.System.NEXT_ALARM_FORMATTED,
                        fmt.format(c.getTime()));
            } else {
                Intent showIntent = new Intent(mContext, MainActivity.class);
                PendingIntent showOperation = PendingIntent.getActivity(mContext, 0, showIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                AlarmManager.AlarmClockInfo alarmClockInfo = new AlarmManager.AlarmClockInfo(c.getTimeInMillis(), showOperation);
                am.setAlarmClock(alarmClockInfo, sender);
            }
        }
    }

    private static void cancelActiveAlarm(Context mContext) {
        Intent intent = new Intent(mContext, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        am.cancel(sender);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            intent = new Intent("android.intent.action.ALARM_CHANGED");
            intent.putExtra("alarmSet", false);
            mContext.sendBroadcast(intent);
            Settings.System.putString(mContext.getContentResolver(),
                    Settings.System.NEXT_ALARM_FORMATTED, "");
        }
    }

    public static void wakeupAlarm(Context mContext) {
        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl =
                pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "AlarmReceiver");
        wl.acquire(5000);
        mContext.startService(new Intent(mContext, AlarmService.class));
    }

    public static void startNapAlarm(Context mContext, NapAlarmItem alarmItem) {
        //Add the used counts of the alarm
        alarmItem.setUsedCounts(alarmItem.getUsedCounts() + 1);
        //update the acitve status
        updateIsActiveInDb(alarmItem, true);
        Intent intent = new Intent(mContext, NapAlarmService.class);
        Bundle bundle = new Bundle();
        bundle.putLong(KEY_ACTIVE_NAP_ALARM_ID, alarmItem.getId());
        intent.putExtras(bundle);
        mContext.startService(intent);
    }

    public static void updateIsActiveInDb(NapAlarmItem item, boolean isActive) {
        item.setIsActive(isActive);
        DbUtil.insertOrUpdateNapAlarm(item);
    }

    public static void dismissAlarm(Context mContext) {
        mContext.stopService(new Intent(mContext, AlarmService.class));
        setNextAlarm(mContext);
    }

    public static void snoozeAlarm(Context mContext) {
        //first stop the service
        dismissAlarm(mContext);
        Log.i(TAG, "Snooze Called");
        PowerManager pm = (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl =
                pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "AlarmSnooze");
        wl.acquire(5000);
        Intent snoozeIntent = new Intent(mContext, SnoozeService.class);
        mContext.startService(snoozeIntent);


        //now set current item's next alarm to 5 min plus
        /*List<AlarmItem> items =  DbUtil.getAlarmItemsByNextAlarmTime(QuickAlarmApp.getState().alarm().alarmTime());
        long snoozeDuration = QuickAlarmApp.getState().settings().snoozeDuration() * 1000 * 60;

        if (items !=null && items.size() > 0) {
            for (AlarmItem item : items) {
                item.setNextAlarm(item.getNextAlarm() + snoozeDuration);
            }
        }
*/
        //now update everything
        //onAlarmEditedTasks(mContext);
    }

    public static void setNextAlarm(Context context) {
        //first cancel any alarm if exist
        cancelActiveAlarm(context);

        if (QuickAlarmApp.getState().alarm().alarmTime() < System.currentTimeMillis()) {
            //first find the current set alarm item. We change its next alarm to the repeat instance
            DbUtil.resetNextAlarms(QuickAlarmApp.getState().alarm().alarmTime());

            long nextAlarm = DbUtil.getNextAlarm();
            //updating state

            QuickAlarmApp.updateAlarmTime(nextAlarm);

            if (nextAlarm > System.currentTimeMillis()) {
                //now setting alarm
                restartAlarm(QuickAlarmApp.getState(), context);
            }
        }
    }

    public static void onAlarmEditedTasks(Context context) {
        //1) find the next alarm
        //2)cancel active alarm
        //Update app state time
        //3) restart the alarm
        long nextAlarm = DbUtil.getNextAlarm();
        long systemTime = System.currentTimeMillis();
        long dateTime = (new Date()).getTime();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, 6);
        cal.set(Calendar.DAY_OF_MONTH, 11);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 30);
        long calTime = cal.getTimeInMillis();
        if (nextAlarm > System.currentTimeMillis()) {
            cancelActiveAlarm(context);
            QuickAlarmApp.updateAlarmTime(nextAlarm);
            restartAlarm(QuickAlarmApp.getState(), context);
        }
    }
}
