package in.mamga.quickalarm.napalarm.events;

import in.mamga.quickalarm.database.NapAlarmItem;

/**
 * Created by shailesh on 15/08/17.
 */

public class NapAlarmStartedEvent {
    public final NapAlarmItem napAlarmItem;

    public NapAlarmStartedEvent(NapAlarmItem napAlarmItem) {
        this.napAlarmItem = napAlarmItem;
    }
}
