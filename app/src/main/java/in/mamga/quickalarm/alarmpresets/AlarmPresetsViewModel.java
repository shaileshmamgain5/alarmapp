package in.mamga.quickalarm.alarmpresets;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import in.mamga.quickalarm.BR;

/**
 * Created by photography on 23/03/17.
 */

public class AlarmPresetsViewModel extends BaseObservable {
    public static final String TAG = "AlarmPresetsViewModel";
    Context context;

    @Bindable
    public boolean alarmPresetsUpdating;

    public AlarmPresetsViewModel(Context context) {
        tryCache(context);
    }

    private void tryCache(Context context) {
        /*WeatherForecastData currentWeatherForecastData = DbUtil.getCurrentWeatherForecastCache(context);
        if (currentWeatherForecastData != null) {
            setCurrentWeatherForecastData(currentWeatherForecastData);
        }*/
    }


    public boolean isAlarmPresetsUpdating() {
        return alarmPresetsUpdating;
    }

    public void setAlarmPresetsUpdating(boolean alarmPresetsUpdating) {
        this.alarmPresetsUpdating = alarmPresetsUpdating;
        notifyPropertyChanged(BR.alarmPresetsUpdating);
    }


    /**
     * Called when view holder fragment or activity is stopped.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onStop(@NonNull String tag) {
    }

    /**
     * Called when view holder fragment or activity is destroyed.
     *
     * @param tag : Name of the fragment in effect
     */
    public void onDestroy(@NonNull String tag) {
        this.context = null;
    }
}
