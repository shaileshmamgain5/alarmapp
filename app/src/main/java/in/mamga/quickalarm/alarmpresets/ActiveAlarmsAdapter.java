package in.mamga.quickalarm.alarmpresets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.mamga.quickalarm.databinding.ItemActiveAlarmsDetailBinding;

/**
 * Created by Shailesh on 13/03/17.
 * Reference article : https://medium.com/google-developers/android-data-binding-recyclerview-db7c40d9f0e4#.jo7xer7of
 */

public class ActiveAlarmsAdapter extends RecyclerView.Adapter<ActiveAlarmsAdapter.WeatherItemViewHolder> {

    Context context;

    public ActiveAlarmsAdapter(Context context) {
        this.context  = context;
    }

    @Override
    public WeatherItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(parent.getContext());
        ItemActiveAlarmsDetailBinding itemActiveAlarmsDetailBinding =
                ItemActiveAlarmsDetailBinding.inflate(inflator, parent, false);
        return new WeatherItemViewHolder(itemActiveAlarmsDetailBinding);
    }

    @Override
    public void onBindViewHolder(WeatherItemViewHolder holder, int position) {
        //holder.bind(weatherForecastData.getList().get(position));
        Log.i("Adapter", "Layout inflated == " + position);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class WeatherItemViewHolder extends RecyclerView.ViewHolder {

        private final ItemActiveAlarmsDetailBinding itemActiveAlarmsDetailBinding;

        public WeatherItemViewHolder(ItemActiveAlarmsDetailBinding itemActiveAlarmsDetailBinding) {
            super(itemActiveAlarmsDetailBinding.getRoot());
            this.itemActiveAlarmsDetailBinding = itemActiveAlarmsDetailBinding;
        }

        public void bind() {
           /* AlarmPresetItemViewModel viewModel = new AlarmPresetItemViewModel(context, weatherItem,  id, true);
            itemWeeklyForcastBinding.setWeatherItemViewModel(viewModel);
            itemWeeklyForcastBinding.executePendingBindings();*/
        }
    }
}