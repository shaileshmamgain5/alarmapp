package in.mamga.quickalarm.alarmhandlers;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Calendar;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.utils.SharePreference;
import in.mamga.quickalarm.utils.Util;

import static in.mamga.quickalarm.utils.StringUtil.SNOOZE_IS_ON_KEY;
import static in.mamga.quickalarm.utils.StringUtil.SNOOZE_LAST_STARTED_KEY;

/**
 * Created by shailesh on 05/08/17.
 */

public class SnoozeService extends Service {
    public static final String TAG = SnoozeService.class.getSimpleName();
    private int SNOOZE_PERIOD = 5 * 60; //in mins, from settings

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        SNOOZE_PERIOD = QuickAlarmApp.getState().settings().snoozeDuration() * 1000 * 60;
        handleAlreadyStartedSnooze();
        startSnooze();
        return START_STICKY;
    }



    private void handleAlreadyStartedSnooze() {
        if (SharePreference.getInstance(this).getBoolean(SNOOZE_IS_ON_KEY)) {
            Long snoozeStartTime = SharePreference.getInstance(this).getLong(SNOOZE_LAST_STARTED_KEY);

            int remTimeIfApplies = getSnoozeRemainingIfApplies(snoozeStartTime);
            if (remTimeIfApplies > 0) {
                SNOOZE_PERIOD = remTimeIfApplies;
            }
        }
    }

    CountDownTimer ctTimer = new CountDownTimer(SNOOZE_PERIOD * 1000, 2500) {
        @Override
        public void onTick(long millisUntilFinished) {
            Log.i(TAG, "Snooze period left " + millisUntilFinished/1000);
        }

        @Override
        public void onFinish() {
            startAlarmActivity();
        }
    };

    private void startAlarmActivity() {
        //saving snooze off
        SharePreference.getInstance(this).putBoolean(SNOOZE_IS_ON_KEY, false);
        //if (!SharePreference.getInstance(this).getBoolean(Constants.IS_ALARM_ACTIVITY_ON)) {
        // Start the activity where you can stop alarm
        AlarmController.wakeupAlarm(this);
        Log.i(TAG, "Alarm activity started " + SNOOZE_PERIOD);
        stopSelf();
        // }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Returns new snooze period if service was killed while in snooze and recreated
     *
     * @param lastStarted
     * @return
     */
    private int getSnoozeRemainingIfApplies(long lastStarted) {
        int snoozeTime = -1;
        Calendar c = Calendar.getInstance();
        long millis = c.getTimeInMillis();

        if (Util.getSecondsInBetween(lastStarted, millis) > SNOOZE_PERIOD) {
            snoozeTime = Util.getSecondsInBetween(lastStarted, millis);
        }
        return snoozeTime;
    }

    private void startSnooze() {
        ctTimer.start();
        //setCurrent time for snooze start
        Calendar c = Calendar.getInstance();
        long millis = c.getTimeInMillis();

        SharePreference.getInstance(this).putBoolean(SNOOZE_IS_ON_KEY, true);
        SharePreference.getInstance(this).putLong(SNOOZE_LAST_STARTED_KEY, millis);
        Log.i(TAG, "Snooze started for period " + SNOOZE_PERIOD);
    }
}
