package in.mamga.quickalarm.alarmhandlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import in.mamga.quickalarm.QuickAlarmApp;


public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmController.restartAlarm(QuickAlarmApp.getState(), context);
    }
}
