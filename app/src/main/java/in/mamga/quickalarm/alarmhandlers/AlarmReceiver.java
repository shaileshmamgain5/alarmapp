package in.mamga.quickalarm.alarmhandlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmController.wakeupAlarm(context);
       // App.dispatch(new Action<>(Actions.Alarm.WAKEUP));
    }
}
