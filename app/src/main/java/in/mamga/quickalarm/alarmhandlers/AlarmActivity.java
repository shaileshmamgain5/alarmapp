package in.mamga.quickalarm.alarmhandlers;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.R;
import in.mamga.quickalarm.ui.Theme;
import in.mamga.quickalarm.utils.Constants;
import in.mamga.quickalarm.utils.SharePreference;


public class AlarmActivity extends Activity {
    public static final String TAG = AlarmActivity.class.getSimpleName();
    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "AlarmActivity");
        mWakeLock.acquire();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        // fill status bar with a theme dark color on post-Lollipop devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Theme.get(0).primaryDarkColor);
        }

        QuickAlarmApp.isAlarmActivityVisible = true;

        SharePreference.getInstance(this).putBoolean(Constants.IS_ALARM_ACTIVITY_ON, true);
        setContentView(R.layout.activity_alarm);
    }

    @Override
    protected void onUserLeaveHint() {
        SharePreference.getInstance(this).putBoolean(Constants.IS_ALARM_ACTIVITY_ON, false);
        super.onUserLeaveHint();
        finish();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onResume() {
        super.onResume();
        finishIfAlarmDismissed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWakeLock.release();
    }

    public void stopAlarm(View v) {
        stopAlarm();
    }

    public void snoozeAlarm(View v) {
        AlarmController.snoozeAlarm(this);
        Log.i(TAG, "Snooze Called");
        finish();
    }

    private void stopAlarm() {
        //App.dispatch(new Action<>(Actions.Alarm.DISMISS));
        AlarmController.dismissAlarm(this);
        finish();
    }
    private void finishIfAlarmDismissed() {
        boolean isRunning = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (AlarmService.class.getName().equals(service.service.getClassName())) {
                isRunning =  true;
            }
        }
       if (!isRunning) {
           finish();
       }
    }
}
