package in.mamga.quickalarm.BindingAdapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by Shailesh on 13/03/17.
 */

public class ImageViewBindingAdapters {

    @BindingAdapter({"android:src", "placeHolder"})
    public static void setResImage(ImageView view, int resId, int placeHolder) {
        if (resId != 0)
            Picasso.with(view.getContext()).load(resId).placeholder(placeHolder).into(view);
    }

    @BindingAdapter({"android:src"})
    public static void setResImage(ImageView view, int resId) {
        if (resId != 0)
            Picasso.with(view.getContext()).load(resId).into(view);
    }
}
