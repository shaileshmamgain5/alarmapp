package in.mamga.quickalarm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Calendar;
import java.util.TimeZone;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;
import in.mamga.quickalarm.activealarms.ActiveAlarmsFragment;
import in.mamga.quickalarm.bedtime.BedtimeFragment;
import in.mamga.quickalarm.dashboard.DashboardFragment;
import in.mamga.quickalarm.dashboard.UpdateClockEvent;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.napalarm.NapAlarmDetailFragment;
import in.mamga.quickalarm.napalarm.events.NapAlarmStartedEvent;
import in.mamga.quickalarm.settings.SettingsFragment;
import in.mamga.quickalarm.settings.ToneChangedEvent;
import in.mamga.quickalarm.utils.Util;

import static in.mamga.quickalarm.napalarm.NapAlarmDetailFragment.KEY_NAP_ALARM_ITEM_ID;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private static final int NAVIGATION_HOME = 0;
    private static final int NAVIGATION_ACTIVE = 1;
    private static final int NAVIGATION_SETTINGS = 2;
    private int currentTab = NAVIGATION_HOME;

    public static final int TONE_PICKER = 102;


    BlurView blurView;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (currentTab != NAVIGATION_HOME) {
                        DashboardFragment dashboardFragment = new DashboardFragment();
                        Bundle bundle = new Bundle();
                        dashboardFragment.setArguments(bundle);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        currentTab = NAVIGATION_HOME;

                        fragmentManager.beginTransaction().replace(R.id.flFragment, dashboardFragment, "Dashboard").commit();
                    }
                    return true;
                case R.id.navigation_active:
                    if (currentTab != NAVIGATION_ACTIVE) {
                        BedtimeFragment bedtimeFragment = new BedtimeFragment();
                        getSupportFragmentManager().beginTransaction().
                                replace(R.id.flFragment, bedtimeFragment, "Bedtime").commit();

                        ActiveAlarmsFragment activeAlarmsFragment = new ActiveAlarmsFragment();
                        Bundle bundle = new Bundle();
                        activeAlarmsFragment.setArguments(bundle);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        currentTab = NAVIGATION_ACTIVE;

                        //fragmentManager.beginTransaction().replace(R.id.flFragment, activeAlarmsFragment, "Alarms").commit();
                    }
                    return true;
                case R.id.navigation_settings:
                    if (currentTab != NAVIGATION_SETTINGS) {
                        SettingsFragment settingsFragment = new SettingsFragment();
                        Bundle bundle = new Bundle();
                        settingsFragment.setArguments(bundle);
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        currentTab = NAVIGATION_SETTINGS;

                        fragmentManager.beginTransaction().replace(R.id.flFragment, settingsFragment, "Settings").commit();
                    }
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.evening_one);
        int dom = Util.getDominantColor(largeIcon);
        if (dom > 0) {
         navigation.setBackgroundColor(dom);
        }

        blurView = (BlurView) findViewById(R.id.blurView);
        final float radius = 20;
        final View decorView = getWindow().getDecorView();
        //Activity's root View. Can also be root View of your layout (preferably)
        final ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        //set background, if your root layout doesn't have one
        final Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .windowBackground(windowBackground)
                .blurAlgorithm(new RenderScriptBlur(this))
                .blurRadius(radius);

        DbUtil.initializeNapAlarmsIfFirstTime(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        startTimer();
        EventBus.getDefault().register(this);
        DashboardFragment dashboardFragment = new DashboardFragment();
        Bundle bundle = new Bundle();
        dashboardFragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        currentTab = NAVIGATION_HOME;

        fragmentManager.beginTransaction().add(R.id.flFragment, dashboardFragment, "Dashboard").commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
        EventBus.getDefault().unregister(this);
    }


    CountDownTimer timer = new CountDownTimer(1000000, 2000) {
        @Override
        public void onTick(long l) {
            Long millis = Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTimeInMillis();
            EventBus.getDefault().post(new UpdateClockEvent(millis));
        }

        @Override
        public void onFinish() {
            startTimer();
        }
    };

    private void startTimer() {
        stopTimer();
        timer.start();
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onNewCitySelected(AlarmSetEvent alarmSetEvent) {

        if (alarmSetEvent.timeInMillis > 0) {
            //Todo : Set Alarm here
        }
        AlarmSetEvent stickyEvent = EventBus.getDefault().getStickyEvent(AlarmSetEvent.class);
        // Better check that an event was actually posted before
        if (stickyEvent != null) {
            // "Consume" the sticky event
            EventBus.getDefault().removeStickyEvent(stickyEvent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case TONE_PICKER:
                    Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
                    Ringtone r = RingtoneManager.getRingtone(this, uri);
                    String ringToneName = r.getTitle(this);
                    QuickAlarmApp.updateRingTone(uri.toString());
                    if (!TextUtils.isEmpty(ringToneName)) {
                        EventBus.getDefault().post(new ToneChangedEvent(ringToneName));
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNapAlarmStarted(NapAlarmStartedEvent event) {
        NapAlarmDetailFragment napAlarmDetailFragment = new NapAlarmDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(KEY_NAP_ALARM_ITEM_ID, event.napAlarmItem.getId());
        napAlarmDetailFragment.setArguments(bundle);
        napAlarmDetailFragment.show(getSupportFragmentManager(), "NapAlarmFragment");
    }

}
