package in.mamga.quickalarm.dashboard;

import android.animation.Animator;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;

import com.kyleduo.switchbutton.SwitchButton;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.R;
import in.mamga.quickalarm.alarmontime.SetAlarmOnTimeViewModel;
import in.mamga.quickalarm.database.AlarmItem;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.databinding.FragmentDashboardBinding;
import in.mamga.quickalarm.napalarm.ActiveNapsAdapter;
import in.mamga.quickalarm.napalarm.events.NapAlarmAddedEvent;
import in.mamga.quickalarm.napalarm.events.NapAlarmDeletedEvent;
import in.mamga.quickalarm.ui.CircularSeekBar;
import in.mamga.quickalarm.ui.ClockView;
import in.mamga.quickalarm.ui.TopSnack;
import in.mamga.quickalarm.utils.Util;

/**
 * Created by Shailesh on 12/03/17.
 */

public class DashboardFragment extends Fragment {
    public static final String TAG = "DashboardFragment";

    DashBoardViewModel dashBoardViewModel;
    RecyclerView rvForecast;
    String cityId;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 9921;

    List<NapAlarmItem> napAlarmItems;
    ActiveNapsAdapter activeNapsAdapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dashboard, parent, false);

        FragmentDashboardBinding dashBoardFragmentBinding = DataBindingUtil.bind(view);

        dashBoardViewModel = new DashBoardViewModel(getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {

        }
        dashBoardFragmentBinding.setDashboardViewModel(dashBoardViewModel);

        dashBoardFragmentBinding.cvAddCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        final ClockView sbHour = (ClockView) view.findViewById(R.id.sbHour);
        ClockView sbMin = (ClockView) view.findViewById(R.id.sbMinute);
        final SwitchButton sbAmPm = (SwitchButton) view.findViewById(R.id.sbAmPm);

        sbAmPm.setChecked(dashBoardViewModel.setAlarmOnTimeViewModel.isAm);

        sbAmPm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    dashBoardViewModel.setAlarmOnTimeViewModel.setAm(true);
                } else {
                    dashBoardViewModel.setAlarmOnTimeViewModel.setAm(false);
                }
                dashBoardViewModel.setAlarmOnTimeViewModel.updateTimeString();
            }
        });

        sbHour.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress != (dashBoardViewModel.setAlarmOnTimeViewModel.hours)) {
                    dashBoardViewModel.setAlarmOnTimeViewModel.setHours(progress);
                    sbHour.invalidate();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbMin.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (QuickAlarmApp.getState().settings().snap()) {
                    progress = (int) (Math.round(progress / 5.0) * 5) % 60;
                }
                if (progress != (dashBoardViewModel.setAlarmOnTimeViewModel.minutes)) {
                    dashBoardViewModel.setAlarmOnTimeViewModel.setMinutes(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        final CardView cvSetAlarm = (CardView) view.findViewById(R.id.cvSetAlarm);

        final CoordinatorLayout layout = (CoordinatorLayout) view.findViewById(R.id.clDashboard);
        dashBoardViewModel.setAlarmOnTimeViewModel.setOnTimeAlarmSetListener(new SetAlarmOnTimeViewModel.OnTimeAlarmSetListener() {
            @Override
            public void onSet(AlarmItem item) {
                TopSnack.getInstance(layout).showSnackbar("Will ring" + Util.getTimeRemaining(item), "Undo", new TopSnack.OnActionListener() {
                    @Override
                    public void onActionClicked() {
                        DbUtil.removeLastSetAlarm();
                        sbAmPm.setChecked(dashBoardViewModel.setAlarmOnTimeViewModel.isAm);
                        startCircleReveal(cvSetAlarm);
                    }
                });
            }
        });
        CircularSeekBar napTimer = (CircularSeekBar) view.findViewById(R.id.napTimeBar);
        napTimer.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                dashBoardViewModel.napAlarmSetViewModel.handleProgressChange(progress);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });
        // set up the RecyclerView
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvNapAlarms);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        napAlarmItems = dashBoardViewModel.getAllNapAlarms();
        if (napAlarmItems == null) {
            napAlarmItems = new ArrayList<>();
        }
        activeNapsAdapter = new ActiveNapsAdapter(napAlarmItems);
        //adapter.setClickListener(this);
        recyclerView.setAdapter(activeNapsAdapter);
        return view;
    }

    private void startCircleReveal(CardView cvSetAlarm) {
        int x = cvSetAlarm.getRight();
        int y = cvSetAlarm.getBottom();

        int startRadius = 0;
        int endRadius = (int) Math.hypot(cvSetAlarm.getWidth(), cvSetAlarm.getHeight());

        Animator anim = ViewAnimationUtils.createCircularReveal(cvSetAlarm, x, y, startRadius, endRadius);

        cvSetAlarm.setVisibility(View.VISIBLE);
        anim.start();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        if (dashBoardViewModel != null) {
            dashBoardViewModel.onStop(TAG);
        }
    }

    /**
     * Receives {@link UpdateClockEvent} from {@link in.mamga.quickalarm.MainActivity}. Called on main thread.
     *
     * @param event the {@link UpdateClockEvent} instance
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTimeClockUpdated(UpdateClockEvent event) {
        if (dashBoardViewModel != null ) {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNapAlarmDeleted(NapAlarmDeletedEvent event) {
        if (dashBoardViewModel != null ) {
            refreshNapItems();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNapAlarmAdded(NapAlarmAddedEvent event) {
        if (dashBoardViewModel != null ) {
            refreshNapItems();
        }
    }
    private void refreshNapItems() {
        if (napAlarmItems != null && activeNapsAdapter != null) {
            napAlarmItems.clear();
            napAlarmItems.addAll(dashBoardViewModel.getAllNapAlarms());
            activeNapsAdapter.notifyDataSetChanged();
        }
    }

}
