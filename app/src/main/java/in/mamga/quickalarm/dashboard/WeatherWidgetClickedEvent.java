package in.mamga.quickalarm.dashboard;

/**
 * Created by Shailesh on 15/03/17.
 */

public class WeatherWidgetClickedEvent {
    public String cityId;
    public Boolean isForecast;
    public int date; //Only if isForecast is true

    public WeatherWidgetClickedEvent (String cityId, Boolean isForecast, int date) {
        this.isForecast = isForecast;
        this.date = date;
        this.cityId = cityId;
    }
}
