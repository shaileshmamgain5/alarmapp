package in.mamga.quickalarm.dashboard;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import java.util.List;

import in.mamga.quickalarm.BR;
import in.mamga.quickalarm.QuickAlarmApp;
import in.mamga.quickalarm.alarmontime.SetAlarmOnTimeViewModel;
import in.mamga.quickalarm.database.DbUtil;
import in.mamga.quickalarm.database.NapAlarmItem;
import in.mamga.quickalarm.napalarm.NapAlarmSetViewModel;

/**
 * Created by Shailesh on 14/03/17.
 */

public class DashBoardViewModel extends BaseObservable {

    public static final String TAG = "DashBoardViewModel";
    Context context;

    public SetAlarmOnTimeViewModel setAlarmOnTimeViewModel;
    public SetAlarmOnTimeViewModel setAlarmOnTimeViewModel1;
    public NapAlarmSetViewModel napAlarmSetViewModel;

    @Bindable
    public boolean isDashboardRefreshing;


    public DashBoardViewModel(Context context) {
        this.context = context;

        setAlarmOnTimeViewModel = new SetAlarmOnTimeViewModel( QuickAlarmApp.getCurrentAlarmItem());
        setAlarmOnTimeViewModel1 = new SetAlarmOnTimeViewModel( QuickAlarmApp.getCurrentAlarmItem());
        napAlarmSetViewModel = new NapAlarmSetViewModel(null);
    }

    public List<NapAlarmItem> getAllNapAlarms() {
        List<NapAlarmItem> list = DbUtil.getAllNapAlarmItems();
        return list;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public void onStop(@NonNull String TAG) {
    }

    public void onDestroy(@NonNull String tag) {
        context = null;
    }


    public void setDashboardRefreshing(Boolean dashboardRefreshing) {
        isDashboardRefreshing = dashboardRefreshing;
        notifyPropertyChanged(BR.isDashboardRefreshing);
    }
}
